# grain_core

#### 介绍
本模块用于小麦管理系统。

#### 软件架构
后端采用:sprintboot + mybatis + mysql/openGauss构成，只提供restfull接口，不提供页面服务。

前端采用swing UI组件编写，直接调用后端restfull接口完成相应的功能。

#### 编译说明

下载jdk1.8+,[链接](https://download.java.net/openjdk/jdk8u41/ri/openjdk-8u41-b04-windows-i586-14_jan_2020.zip)， 并参考[链接](https://blog.csdn.net/weixin_45078706/article/details/115830318)， 完成java的环境变量配置。

下载maven 3.6.3版本[链接](https://archive.apache.org/dist/maven/maven-3/)，并参考[链接](https://blog.csdn.net/m0_53370922/article/details/119829116)完成maven的环境变量和镜像源配置。

下载本页面的代码到任意目录$HOME(本次假定为d:/grain_core) ,进入$HOME,执行`mvn clean install package -Dmaven.test.skip=true`即完成代码编译。

前端UI在$HOME/grain_ui/target下，grain_ui-1.0.0.jar, 其依赖包在lib目录下。二者拷贝到任意目录后执行`java -jar grain_ui-1.0.0.jar`即可启动UI。

后端服务在$HOME/grain-web/target下，grain-web-1.0.0.jar, 它没有依赖，也通过`java -jar grain-web-1.0.0.jar`来运行。



#### 安装说明

1. 创建数据库(以下任选其一，mysql可以在windows下安装)

   方式1: 在centOS或openEuler linux操作系统上安装openGauss,参考官方手册:[链接](https://opengauss.org/zh/docs/3.0.0/docs/installation/%E6%9E%81%E7%AE%80%E7%89%88%E5%AE%89%E8%A3%85.html)

   方式2:安装mysql 8.0以上版本，参考[链接](https://product.pconline.com.cn/itbk/software/rjwt/1511/7213184.html)

2. 配置数据库

   a. 为数据库创建名为grain_system的数据库，用户名为test,密码为Test@123，配置数据库的使用端口为14000

   b. 使用数据库命令行工具执行SQL.sql,生成对应的数据表。

3. 启动后端服务

   a. 使用ipconfig确认数据库的ip

   b. 进入后端服务源代码目录，修改application.yml文件中的spring.datasource.url中的ip和端口，如果是opengauss数据库不需要其他修改；如果是mysql需要将驱动串修改为jdbc:mysql://。

   c. 执行`java -jar grain-web-1.0.0.jar`即启动后端服务，正确启动日志如下图:

   ![1650557004592](README/1650557004592.png "启动服务")

4. 启动前端服务

   前端服务目录下需要复制一份url.properties文件，将host.ip的值配置为服务端IP，然后执行`java -jar grain_ui-1.0.0.jar`即可启动前端UI：

   ![1650557162552](README/1650557162552.png "主UI")

   #### 使用说明

   我将整个过程分为3个步骤，第一个步骤主要为了完成身份信息录入、车辆信息录入和重量录入；第二步骤为了完成小麦的质量录入；第三步骤为录入去除小麦后的车重和袋皮重，并且此时录入完成后可以查阅订单。

   ##### 步骤1 UI介绍

   ![1650557414920](README/1650557414920.png "step1")

   若该用户之前已经卖过小麦，那么他的身份证信息没有必要全部录入，只需要录入后3~6位系统自动从后端数据库中查出用户，直接选择即可；车辆信息车牌号和描述至少有一个不能为空；再录入车+小麦总重后，即可生成订单,同时打印生成的订单号。录入一个订单信息后可以通过重置后录入下一个订单号。

   ![1650557660258](README/1650557660258.png "生成订单")

   ##### 步骤2 UI介绍

   在上一步用户订单生成之后， 另个节点会进入步骤2的UI:

   ![1650557869757](README/1650557869757.png)

   用户可以通过身份证号(任意后6位搜索或者全输入搜索)后确认身份信息，再通过身份信息搜索其关联的订单(即刚刚称重还未进行小麦质量录入的订单)；也可以从上一步骤中直接得到的订单号直接搜索指定订单。注意此步骤中锁定订单后的用户身份证号、名称和车辆信息是否正确，如不正确可以点击重置重新查找订单；

   当用户选定订单后，即可录入小麦质量信息，优先录入容重后，其他的标准会自动带出，确认无误后更新订单:

   ![1650558104533](README/1650558104533.png "步骤2订单")

   用户可以点击重置后录入下一个订单。

   ##### 步骤3 UI介绍

   第三个步骤中，用户像上面的搜索步骤一样，搜索并锁定需要操作的订单：

   ![1650558234301](README/1650558234301.png)

   录入车重和袋皮重后，订单流程结束，此时可以点击打印订单，查看此订单报表(对接打印机功能未实现，可以截图打印或者对照着手抄):

   ![1650558313757](README/1650558313757.png)

   已经完成的订单(即步骤3里已经更新了车重和袋皮重)，需要在此界面点击查完结订单切换为搜索完结单模式，此时只能按照身份证或者订单号搜索到完结问题单，并点击打印订单查看详情界面：

   ![1650558509072](README/1650558509072.png)

   

#### 参与贡献

无