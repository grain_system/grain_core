package org.grain.http;

import java.sql.Timestamp;
import java.util.List;

import org.grain.common.constant.State;
import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.utils.IJson;
import org.grain.utils.JsonConvert;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrainRequestService {
	private Request request = new Request();
	
	public List<User> searchUsers(String userId) throws Exception {
		User query = new User();
		query.setId(userId);
	    return JsonConvert.deserialList(User.class, request.getJSONArray(Request.KEY_USER_SEARCH, query));
	}
	
	public OperatorReason addUser(User user) {
		return jsonRequst(Request.KEY_USER_ADD, user);
	}
	
	public OperatorReason addCar(Car car) {
		return jsonRequst(Request.KEY_CAR_ADD, car);
	}
	
	public Car getCar(long carId) throws Exception {
		Car car = new Car();
		car.setCarId(carId);
		return JsonConvert.deserialList(Car.class, request.getJSONArray(Request.KEY_CAR_GET, car)).get(0);
	}
	
	public OperatorReason addOrderStep1(Order order) {
		return jsonRequst(Request.KEY_ORDER_STEP1, order);
	}
	
	public OperatorReason addOrderStep2(Order order) {
		return jsonRequst(Request.KEY_ORDER_STEP2, order);
	}

	public OperatorReason addOrderStep3(Order order) {
		return jsonRequst(Request.KEY_ORDER_STEP3, order);
	}
	
	public OperatorReason addOrderStep4(Order order) {
		return jsonRequst(Request.KEY_ORDER_STEP4, order);
	}

	public OperatorReason deleteOrder(Order order) {
		return jsonRequst(Request.KEY_ORDER_DELORDER, order);
	}

	public List<Order> searchOrders(String owner, State state, Timestamp createTime) throws Exception {
		Order order = new Order();
		order.setOwner(owner);
		order.setState(state);
		order.setDoQualityTime(createTime);
		return searchOrders(order);
	}
	
	public List<Order> searchOrders(long orderId, State state) throws Exception {
		Order order = new Order();
		order.setId(orderId);
		order.setState(state);
		return searchOrders(order);
	}
	
	public List<Order> searchOrders(Order order) throws Exception {
		return JsonConvert.deserialList(Order.class, request.getJSONArray(Request.KEY_ORDER_SEARCH, order));
	}
	
	public OperatorReason jsonRequst(String key, IJson ijson) {
		try {
			return request.getOperationReason(key, ijson);
		} catch (Exception exp) {
			exp.printStackTrace();
			log.error("request key:" + key + " failed, reason=" + exp.getMessage());
			return OperatorReasonEnum.EXCEPTION.get(exp);
		}
	}
}
