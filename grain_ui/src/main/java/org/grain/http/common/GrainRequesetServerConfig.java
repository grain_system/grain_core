package org.grain.http.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GrainRequesetServerConfig {
	private String protocal;
	private String host;
	private String port;
	
	public String getGetUrl(GrainRequestConfig config) {
		return String.format("%s://%s:%s%s", protocal, host, port, config.getUrl());
	}
}
