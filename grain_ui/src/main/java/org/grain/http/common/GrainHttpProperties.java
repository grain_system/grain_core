package org.grain.http.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GrainHttpProperties {
	public static final String PROP_FILE = "url.properties";
	private GrainRequesetServerConfig grainRequesetServerConfig;
	private Map<String, GrainRequestConfig> requestConfigMap = new ConcurrentHashMap<>();
	private int connectTimeout = 5000;
	private int socketTimeout = 3000;
	
	public GrainHttpProperties() {
		Properties props = loadProperties();
		initHttpRequestConfig(props);
		initServerConfig(props);
		initRequestConfig(props);
	}
	
	public int getConnectTimeout() {
		return connectTimeout;
	}
	
	public int getSocketTimeout() {
		return socketTimeout;
	}

	public GrainRequestConfig getRequestConfig(String key) {
		return requestConfigMap.get(key);
	}

	public GrainRequesetServerConfig getServerConfig() {
		return grainRequesetServerConfig;
	}
	
	private void initHttpRequestConfig(Properties props) {
		connectTimeout = Integer.parseInt(props.getProperty("request.connectTimeout"));
		socketTimeout = Integer.parseInt(props.getProperty("request.socketTimeout"));
	}

	private void initServerConfig(Properties props) {
		String[] hostConfigs = {"protocal", "ip", "port"};
		List<String> hostProps = Arrays.stream(hostConfigs)
				.map(key -> props.getProperty(String.format("host.%s", key)))
				.collect(Collectors.toList());
		grainRequesetServerConfig = new GrainRequesetServerConfig(hostProps.get(0), hostProps.get(1), hostProps.get(2));
	}

	private void initRequestConfig(final Properties props) {
		final String URL_PREFIX = "url.";
	    List<String> urlKeys = new LinkedList<>();
	    Enumeration<?> enumKey = props.propertyNames();
	    while (enumKey.hasMoreElements()) {
	    	String key = enumKey.nextElement().toString();
	    	if (key.startsWith(URL_PREFIX)) {
	    		urlKeys.add(key.substring(URL_PREFIX.length()));
			}
		}

	    for (String urlKey: urlKeys) {
	    	String propUrlKey = URL_PREFIX + urlKey;
	    	String url = props.getProperty(propUrlKey);
	    	String method = props.getProperty(String.format(urlKey + ".method"), "GET");
			GrainRequestConfig config = new GrainRequestConfig(url, method);
	    	requestConfigMap.put(URL_PREFIX + urlKey, config);
		}
	}

    private static Properties localUserProperties() {
        String filePath = System.getProperty("user.dir")
            + File.separator
            + PROP_FILE;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(filePath));
            return properties;
        }catch (Exception exception){
        	exception.printStackTrace();
            return null;
        }
    }
    
    public static Properties loadProperties() {
        Properties prop = localUserProperties();
        if (prop != null) {
        	return prop;
        }
        prop = new Properties();

        try (InputStream is = GrainHttpProperties.class.getClassLoader().getResourceAsStream(PROP_FILE)) {
            prop.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
