package org.grain.http.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GrainRequestConfig {
	private String url;
	private String method;
}
