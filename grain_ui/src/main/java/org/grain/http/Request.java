package org.grain.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.impl.OperationReasonImpl;
import org.grain.http.common.GrainHttpProperties;
import org.grain.http.common.GrainRequesetServerConfig;
import org.grain.http.common.GrainRequestConfig;
import org.grain.utils.IJson;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Request {
	public static final String CHARSET = "UTF-8";
	public static final String KEY_USER_ADD = "url.useradd";
	public static final String KEY_USER_INSERT = "url.userinsert";
	public static final String KEY_USER_SEARCH = "url.usersearch";
	public static final String KEY_CAR_ADD = "url.caradd";
	public static final String KEY_CAR_GET = "url.carget";
	public static final String KEY_ORDER_STEP1 = "url.orderstep1";
	public static final String KEY_ORDER_STEP2 = "url.orderstep2";
	public static final String KEY_ORDER_STEP3 = "url.orderstep3";
	public static final String KEY_ORDER_STEP4 = "url.orderstep4";
	public static final String KEY_ORDER_DELORDER = "url.orderdelorder";
	public static final String KEY_ORDER_SEARCH = "url.ordersearch";

	private RequestConfig config;
	private HttpClient httpclient;
	private GrainHttpProperties grainHttpProperties;
	private GrainRequesetServerConfig serverConfig;

	public Request() {
		grainHttpProperties = new GrainHttpProperties();
		serverConfig = grainHttpProperties.getServerConfig();
		config = RequestConfig.custom()
				.setConnectTimeout(grainHttpProperties.getConnectTimeout())
				.setSocketTimeout(grainHttpProperties.getSocketTimeout())
				.build();
		httpclient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
	}

	public JSONArray getJSONArray(String key, IJson ijson) throws Exception {
		String entity = request(key, ijson);
		return JSONUtil.parseArray(entity);

	}
	
	public OperatorReason getOperationReason(String key, IJson isjon) throws Exception {
		String entity = request(key, isjon);
		return OperationReasonImpl.build(JSONUtil.parseObj(entity));
	}
	
	public String request(String key, IJson ijson) throws Exception {
		GrainRequestConfig config = grainHttpProperties.getRequestConfig(key);
		return request(serverConfig.getGetUrl(config), ijson, config.getMethod());

	}
	
	public String request(String url, IJson ijson, String method) throws Exception {
		String entity = "";
		Map<String, Object> params = ijson == null ? null : ijson.toMapObject();
		if (HttpGet.METHOD_NAME.equals(method)) {
			entity = doGet(url, params);
		} else {
			entity = doPost(url, params);
		}
		return entity;
	}
	
	
	public String doGet(String url, Map<String, Object> params) throws Exception {
		if(params !=null && !params.isEmpty()){
			
			List<NameValuePair> pairs = new ArrayList<NameValuePair>(params.size());
			
			for (String key :params.keySet()){
				pairs.add(new BasicNameValuePair(key, params.get(key).toString()));
			}
//			url +="?"+EntityUtils.toString(new UrlEncodedFormEntity(pairs), CHARSET);
			url +="?"+EntityUtils.toString(new UrlEncodedFormEntity(pairs, CHARSET), CHARSET);
		}
		
		log.info("doGet url=" + url);
		HttpGet httpGet = new HttpGet(url);
		HttpResponse response = httpclient.execute(httpGet);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode !=200){
			httpGet.abort();
			throw new RuntimeException("HttpClient,error status code :" + statusCode);
		}
		HttpEntity entity = response.getEntity();
        String result = null;
        if (entity != null) {
            result = EntityUtils.toString(entity, "utf-8");
            EntityUtils.consume(entity);
//            response.close();
			log.info("doGet url=" + url + ", result=" + result);
            return result;
        }else{
			log.warn("doGet url=" + url + ", result is null");
        	return null;
        }
	}
	
	public String doPost(String url, Map<String, Object> params) throws Exception {
		List<NameValuePair> pairs = null;
		if (params != null && !params.isEmpty()) {
			pairs = new ArrayList<NameValuePair>(params.size());
			for (String key : params.keySet()) {
				pairs.add(new BasicNameValuePair(key, params.get(key).toString()));
			}
		}
		HttpPost httpPost = new HttpPost(url);
		if (pairs != null && pairs.size() > 0) {
			httpPost.setEntity(new UrlEncodedFormEntity(pairs, CHARSET));
		}
		log.info("doPost url=" + url);
		HttpResponse response = httpclient.execute(httpPost);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != 200) {
			httpPost.abort();
			throw new RuntimeException("HttpClient,error status code :" + statusCode);
		}
		HttpEntity entity = response.getEntity();
		String result = null;
		if (entity != null) {
			result = EntityUtils.toString(entity, "utf-8");
			EntityUtils.consume(entity);
//		        response.close();
			log.info("doPost url=" + url + ", entity=" + entity);
			return result;
		}else{
			log.warn("doPost url=" + url + ", result is null");
			 return null;
		}
	}
}
