package org.grain;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.grain.view.step1.Step1NewMainDlg;
import org.grain.view.step2.Step2MainDlg;
import org.grain.view.step3.Step3MainDlg;
import org.grain.view.step4.Step4MainDlg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GrainUi extends JDialog {
	
	private static final Logger ilog = LoggerFactory.getLogger(GrainUi.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GrainUi dialog = new GrainUi();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public GrainUi() {
		setTitle("Grain选择界面");
		setBounds(100, 100, 406, 393);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(4, 0, 0, 0));
		
		JButton btnStep1Button = new JButton("步骤1-质量录入");
		btnStep1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ilog.info("step1 clicked!");
				Step1NewMainDlg dlg = new Step1NewMainDlg(GrainUi.this, true);
				dlg.setVisible(true);
			}
		});
		panel.add(btnStep1Button);
		
		JButton btnStep2Button = new JButton("步骤2-毛重录入");
		btnStep2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ilog.info("step2 clicked!");
				Step2MainDlg dlg = new Step2MainDlg(GrainUi.this, true);
				dlg.setVisible(true);
			}
		});
		panel.add(btnStep2Button);
		
		JButton btnStep3Button = new JButton("步骤3-车重和打印");
		btnStep3Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ilog.info("step3 clicked!");
				Step3MainDlg dlg = new Step3MainDlg(GrainUi.this, true);
				dlg.setVisible(true);
			}
		});
		panel.add(btnStep3Button);
		
		JButton btnStep4Button = new JButton("订单管理(管理员专用)");
		btnStep4Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ilog.info("step4 clicked!");
				Step4MainDlg dlg = new Step4MainDlg(GrainUi.this, true);
				dlg.setVisible(true);
			}
		});
		panel.add(btnStep4Button);

	}

}
