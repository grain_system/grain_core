package org.grain.view.helperimpl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.grain.common.constant.Sex;
import org.grain.common.domain.po.User;
import org.grain.view.RowHelper;

public class UserRowHelper implements RowHelper{
	private static final List<String> titles = Arrays.asList("身份证号", "名字", "性别", "民族","出生日期")
			.stream()
			.collect(Collectors.toList());
	
	private static final List<Integer> titlesWeight = Arrays.asList(200, 100, 50, 50, 100)
			.stream()
			.collect(Collectors.toList());

	@Override
	public Object getColumn(Object obj, int column) {
		// TODO Auto-generated method stub
		if (!(obj instanceof User)) return null;
		User user = (User) obj;
		if (column == 0) {
			return user.getId();
		} else if (column == 1) {
			return user.getName();
		} else if (column == 2) {
			Sex sex = user.getSex();
			return sex == null ? null : sex.getDesc();
		} else if (column == 3) {
			return user.getNation();
		} else if (column == 4) {
			return user.getBirthday();
		} else {
			return null;
		}
	}

	@Override
	public List<String> getTitles() {
		// TODO Auto-generated method stub
		return titles;
	}

	@Override
	public Integer getTitlesWidth(int column) {
		// TODO Auto-generated method stub
		return titlesWeight.get(column);
	}

}
