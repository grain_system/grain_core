package org.grain.view.helperimpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import java.sql.Date;

import org.grain.common.constant.Sex;
import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;
import org.grain.http.GrainRequestService;
import org.grain.view.RowHelper;

public class OrderRowHelper implements RowHelper {
	private static final List<String> titles = Arrays.asList(
			"订单号", "总重量", "创建时间", 
			"名字", "性别",
			"车牌号", "车类型", "车颜色", "车描述",
			"民族", "出生日期")
			.stream()
			.collect(Collectors.toList());
	
	private static final List<Integer> titlesWeight = Arrays.asList(20, 20, 110, 30, 10, 30, 30, 30, 60, 20, 30)
			.stream()
			.collect(Collectors.toList());
	
	private GrainRequestService grainRequestService;
	private int orderSize;
	private User user;
	private Map<Long, Car> mapCars;
	public OrderRowHelper(GrainRequestService grainRequestService, int orderSize, User user) {
		this.grainRequestService = grainRequestService;
		this.user = user;
		this.orderSize = orderSize;
		mapCars = new ConcurrentHashMap<Long, Car>(orderSize);
	}
	
	public User getUser() {
		return user;
	}

	public Car getCar(Order order) {
		long orderId = order.getId();
		if (mapCars.containsKey(orderId)) {
			return mapCars.get(orderId);
		}
		synchronized (this) {
			if (mapCars.containsKey(orderId)) {
				return mapCars.get(orderId);
			}
			Car queryCar = null;
			try {
				queryCar = grainRequestService.getCar(order.getCarId());
			} catch (Exception e) {
				queryCar = new Car();
				queryCar.setCarDesc("未知车辆");
				queryCar.setCardNum("未知车牌");
			}
			mapCars.put(orderId, queryCar);
			return queryCar;
		}
	}

	@Override
	public Object getColumn(Object obj, int column) {
		// TODO Auto-generated method stub
		if (!(obj instanceof Order)) return null;
		Order order = (Order) obj;
		if (column == 0) {
			return order.getId();
		} else if (column == 1) {
			return order.getTotalWeight();
		} else if (column == 2) {
			return order.getDoQualityTime();
		} else if (column == 3) {
			return user.getName();
		} else if (column == 4) {
			Sex sex = user.getSex();
			return sex == null ? null : sex.getDesc();
		} else if (column == 5) {
			return getCar(order).getCardNum();
		} else if (column == 6) {
			return getCar(order).getCarType();
		} else if (column == 7) {
			return getCar(order).getCarColor();
		} else if (column == 8) {
			return getCar(order).getCarDesc();
		} else if (column == 9) {
			return user.getNation();
		} else if (column == 10) {
			return user.getBirthday();
		} else {
			return null;
		}
	}

	@Override
	public List<String> getTitles() {
		// TODO Auto-generated method stub
		return titles;
	}

	@Override
	public Integer getTitlesWidth(int column) {
		// TODO Auto-generated method stub
		return titlesWeight.get(column);
	}

}
