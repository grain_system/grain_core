package org.grain.view;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Pattern;

import javax.swing.text.JTextComponent;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IntKeyAdapt extends KeyAdapter {
	private static Pattern validPattern = Pattern.compile("[0-9]");
	private static Pattern validFullPattern = Pattern.compile("[0-9]*");
	protected JTextComponent textComp;
	public IntKeyAdapt(JTextComponent textComp) {
		this.textComp = textComp;
	}

	boolean ctrlTyped = false;
	@Override
	public void keyTyped(KeyEvent e) {
		log.info("key typed 1 " + KeyEvent.getKeyText(e.getKeyCode()));
		if (e.getModifiers() == KeyEvent.CTRL_MASK) {
			ctrlTyped = true;
		} else {
			ctrlTyped = false;
		}
		Character keyCh = e.getKeyChar();
		Pattern pat = getNumberPattern();
		if (!pat.matcher(String.valueOf(keyCh)).matches()) {
			if (keyCh != ' ') {// 回车字符
				e.setKeyChar('\0');
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (ctrlTyped) {
			if (!getFullNumberPattern().matcher(textComp.getText()).matches()) {
				textComp.setText("");
			}
		}
		
	}
	
	protected Pattern getNumberPattern() {
		return validPattern;
	}
	
	protected Pattern getFullNumberPattern() {
		return validFullPattern;
	}
}
