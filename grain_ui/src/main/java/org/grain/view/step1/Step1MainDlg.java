package org.grain.view.step1;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.Dialog;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.border.LineBorder;

import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.grain.common.constant.Sex;
import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.http.GrainRequestService;
import org.grain.view.PopupComboxDlg;
import org.grain.view.helperimpl.UserRowHelper;

import lombok.extern.slf4j.Slf4j;

import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@Slf4j
public class Step1MainDlg extends JDialog {
	private JTextField birthTextField;
	private JTextField idNumTextField;
	private JTextField idNameTextField;
	private JTextField idNationtextField;
	private JTextField carNumTextField;
	private JTextField weightTextField;
	private GrainRequestService grainRequestService;
	private JTextPane textShowInfoPane;
	private JRadioButton rdbtnSexMaleRadioButton;
	private JRadioButton rdbtnFemaleRadioButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Step1MainDlg dialog = new Step1MainDlg();
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public Step1MainDlg() {
		this(null, false);
	}

	public Step1MainDlg(Dialog parent, boolean modal) {
		super(parent, "", modal);
		setTitle("步骤1选择界面");
		setBounds(100, 100, 558, 579);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		grainRequestService = new GrainRequestService();
		getContentPane().setLayout(new GridLayout(1, 0, 1, 0));
		
		JPanel rootPanel = new JPanel();
		getContentPane().add(rootPanel);
		rootPanel.setLayout(new GridLayout(4, 0, 0, 0));
		
		JPanel idPanel = new JPanel();
		idPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 0, 0)), "\u8EAB\u4EFD\u4FE1\u606F\u5F55\u5165", TitledBorder.LEADING, TitledBorder.TOP, null, Color.RED));
		rootPanel.add(idPanel);
		idPanel.setLayout(new GridLayout(3, 0, 2, 2));
		
		JPanel idNumPanel = new JPanel();

		JPanel idNumLabelPanel = new JPanel();
		idNumPanel.add(idNumLabelPanel);
		idNumLabelPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel idNumLabel = new JLabel("身份证号(*):");
		idNumLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idNumLabelPanel.add(idNumLabel);

		idPanel.add(idNumPanel);
		idNumPanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		idNumTextField = new JTextField();
		idNumTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				log.debug("key typed " + e.getKeyChar());
				Character keyCh = e.getKeyChar();
				Pattern pat = Pattern.compile("[0-9]|[X]");
                if (!pat.matcher(String.valueOf(keyCh)).matches()) { 
                    if   (keyCh  !=   ' ') {//回车字符 
                        e.setKeyChar( '\0');
                    }
                }
                if (idNumTextField.getText().length() >= 20) {
                	e.setKeyChar('\0');
                }
			} 
			@Override
			public void keyReleased(KeyEvent e) {
                updateBirthDay();
			}
		});
		idNumPanel.add(idNumTextField);
		idNumTextField.setColumns(10);
		
		JButton btnIdNumButton = new JButton("搜索");
		btnIdNumButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnIdNumButton.setEnabled(false);
				try {
					String number = idNumTextField.getText();
					if (number == null || number.length() < 3) {
						JOptionPane.showMessageDialog(Step1MainDlg.this, "请至少输入3位身份证号!", "标题",JOptionPane.WARNING_MESSAGE); 
						return;
					}
					List<User> users = grainRequestService.searchUsers(number);
					log.info("the user count:" + users.size());
					if (users.isEmpty()) {
						textShowInfoPane.setText("未找到匹配用户,请确认!");
						JOptionPane.showMessageDialog(Step1MainDlg.this, "未找到匹配用户,请确认", "标题",JOptionPane.WARNING_MESSAGE); 
						return;
					}
					PopupComboxDlg popupComboxDlg = new PopupComboxDlg(Step1MainDlg.this, true, (List<?>) users, new UserRowHelper());
					popupComboxDlg.setVisible(true);
					
					if (popupComboxDlg.isSelect() && popupComboxDlg.getSelectObj() != null) {
						User user = (User) popupComboxDlg.getSelectObj();
						log.info("select the user!" + user.toString());
						updateUserInfo(user);
					}
					
				} catch (Exception exp) {
					log.error("search user failed!" + exp.toString());
					textShowInfoPane.setText("查找身份证号出现异常" + exp.toString());

				}finally {
					btnIdNumButton.setEnabled(true);
				}
			}
		});
		idNumPanel.add(btnIdNumButton);
		
		JPanel idNamePanel = new JPanel();
		idPanel.add(idNamePanel);
		idNamePanel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new JPanel();
		idNamePanel.add(panel);

		JLabel lblIdNameLabel = new JLabel("姓名(*):");
		panel.add(lblIdNameLabel);
		
		JPanel panel_1 = new JPanel();
		idNamePanel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		idNameTextField = new JTextField();
		panel_1.add(idNameTextField);
		idNameTextField.setColumns(10);
		
		JPanel panel_7 = new JPanel();
		idNamePanel.add(panel_7);
		panel_7.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("生日");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_7.add(lblNewLabel);
		
		JPanel panel_8 = new JPanel();
		idNamePanel.add(panel_8);
		panel_8.setLayout(new GridLayout(0, 1, 0, 0));
		
		birthTextField = new JTextField();
		birthTextField.setEditable(false);
		birthTextField.setHorizontalAlignment(SwingConstants.LEFT);
		panel_8.add(birthTextField);
		birthTextField.setColumns(10);
		
		
		JPanel sexAndNationPanel = new JPanel();
		idPanel.add(sexAndNationPanel);
		sexAndNationPanel.setLayout(new GridLayout(1, 2, 2, 0));
		
		JPanel panel_2 = new JPanel();
		sexAndNationPanel.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblSexLabel = new JLabel("性别");
		lblSexLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblSexLabel);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		
		rdbtnSexMaleRadioButton = new JRadioButton("男");
		rdbtnSexMaleRadioButton.setSelected(true);
		rdbtnSexMaleRadioButton.setHorizontalAlignment(SwingConstants.CENTER);
		
		rdbtnFemaleRadioButton = new JRadioButton("女");
		rdbtnFemaleRadioButton.setHorizontalAlignment(SwingConstants.CENTER);
		
		ButtonGroup sexBtnGroup = new ButtonGroup();
		sexBtnGroup.add(rdbtnSexMaleRadioButton);
		sexBtnGroup.add(rdbtnFemaleRadioButton);

		panel_6.setLayout(new GridLayout(0, 2, 0, 0));
		panel_6.add(rdbtnSexMaleRadioButton);
		panel_6.add(rdbtnFemaleRadioButton);
		
		JPanel panel_3 = new JPanel();
		sexAndNationPanel.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5);
		panel_5.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("民族:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_5.add(lblNewLabel_1);
		
		idNationtextField = new JTextField();
		panel_5.add(idNationtextField);
		idNationtextField.setColumns(10);
		
		JPanel carPanel = new JPanel();
		carPanel.setBorder(new TitledBorder(new LineBorder(new Color(0, 255, 0)), "\u8F66\u8F86\u4FE1\u606F\u5F55\u5165", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		rootPanel.add(carPanel);
		carPanel.setLayout(new GridLayout(2, 0, 0, 0));
		
		JPanel panel_9 = new JPanel();
		carPanel.add(panel_9);
		panel_9.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel panel_15 = new JPanel();
		panel_9.add(panel_15);
		panel_15.setLayout(new GridLayout(0, 3, 2, 0));
		
		JPanel panel_11 = new JPanel();
		panel_15.add(panel_11);
		panel_11.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_2 = new JLabel("车牌号:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_11.add(lblNewLabel_2);
		
		carNumTextField = new JTextField();
		carNumTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				carNumTextField.setText(carNumTextField.getText().toUpperCase());
			}
		});
		carNumTextField.setHorizontalAlignment(SwingConstants.LEFT);
		panel_11.add(carNumTextField);
		carNumTextField.setColumns(10);
		
		JPanel panel_12 = new JPanel();
		panel_15.add(panel_12);
		panel_12.setLayout(new GridLayout(0, 2, 0, 0));

		
		JLabel lblNewLabel_3 = new JLabel("车辆类型:");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		panel_12.add(lblNewLabel_3);
		
		carTypecomboBox = new JComboBox();
		String[] carTypes = {"拖拉机", "三轮车", "摩托车", "小汽车", "卡车", "货车", "畜力车", "自行车", "其他"};
		for (String carType: carTypes) {
			carTypecomboBox.addItem(carType);
		}
		panel_12.add(carTypecomboBox);
		
		JPanel panel_13 = new JPanel();
		panel_15.add(panel_13);
		panel_13.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_4 = new JLabel("车辆颜色:");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		panel_13.add(lblNewLabel_4);
		
		carColorTextField = new JTextField();
		panel_13.add(carColorTextField);
		carColorTextField.setColumns(10);
		
		Component verticalGlue = Box.createVerticalGlue();
		panel_9.add(verticalGlue);
		
		
		JPanel panel_10 = new JPanel();
		carPanel.add(panel_10);
		panel_10.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new TitledBorder(new LineBorder(new Color(192, 192, 192)), "\u8F66\u8F86\u63CF\u8FF0\u4FE1\u606F(\u6709\u8F66\u724C\u53F7\u6B64\u5904\u53EF\u4E0D\u586B)", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_10.add(panel_14);
		panel_14.setLayout(new GridLayout(0, 1, 0, 0));
		
		editorPane = new JEditorPane();
		panel_14.add(editorPane);
		
		JPanel weightPanel = new JPanel();
		weightPanel.setBorder(new TitledBorder(new EmptyBorder(0, 0, 0, 0), "\u5C0F\u9EA6\u91CD\u91CF\u5F55\u5165", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		rootPanel.add(weightPanel);
		weightPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_16 = new JPanel();
		weightPanel.add(panel_16);
		panel_16.setLayout(new GridLayout(0, 4, 5, 0));
		
		JLabel lblNewLabel_5 = new JLabel("总重量录入:");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		panel_16.add(lblNewLabel_5);
		
		weightTextField = new JTextField();
		weightTextField.addKeyListener(new KeyAdapter() {
			private boolean haveDot = false;
			@Override
			public void keyTyped(KeyEvent e) {
				log.debug("key typed " + e.getKeyChar());
				Character keyCh = e.getKeyChar();
				Pattern pat = Pattern.compile("[0-9]|[.]");
                if (!pat.matcher(String.valueOf(keyCh)).matches()) { 
                    if   (keyCh  !=   ' ') {//回车字符 
                        e.setKeyChar( '\0');
                    }
                }
                if (keyCh == '.' && haveDot) {
                	e.setKeyChar('\0');
                }
			} 
			@Override
			public void keyReleased(KeyEvent e) {
				Pattern pat = Pattern.compile("([0-9]|[.]){0,}");
				String inputText = weightTextField.getText();
				if (!pat.matcher(inputText).matches()) {
					weightTextField.setText("");
					log.info("input number invalid");
					return;
				}
				if (StringUtils.countMatches(inputText, '.') > 1) {
					weightTextField.setText("");
					log.info("input number have more than one dot.");
					return;
				}
				if (weightTextField.getText().contains(".")) {
					haveDot = true;
				} else {
					haveDot = false;
				}
			}
		});
		panel_16.add(weightTextField);
		weightTextField.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("(单位,千克-kg)");
		panel_16.add(lblNewLabel_6);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel_16.add(horizontalGlue);
		
		Component verticalGlue_1 = Box.createVerticalGlue();
		weightPanel.add(verticalGlue_1);
		
		textShowInfoPane = new JTextPane();
		textShowInfoPane.setEditable(false);
		textShowInfoPane.setForeground(Color.BLACK);
		textShowInfoPane.setText("提示信息。");
		textShowInfoPane.setFont(new Font("宋体", Font.PLAIN, 12));
		weightPanel.add(textShowInfoPane);
		
		JPanel textShowPanel = new JPanel();
		rootPanel.add(textShowPanel);
		textShowPanel.setLayout(new GridLayout(3, 1, 0, 0));
		
		JPanel panel_17 = new JPanel();
		textShowPanel.add(panel_17);
		panel_17.setLayout(new GridLayout(0, 5, 0, 0));
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_1);
		
		JButton btnResetButton = new JButton("重置");
		btnResetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearAllIputComponent();
				btnNewOrderButton.setEnabled(true);
			}
		});
		panel_17.add(btnResetButton);
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_2);
		
		btnNewOrderButton = new JButton("生成订单");
		btnNewOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User user = loadUser();
				if (user == null || !user.isValidUser()) {
					String loginfo = "user input error, id and name must configurate, and length(18)!";
					String message = "用户信息配置不正确,身份证号长度为18位且只能有1个X,名字不能为空!";
					showInfoAndMessage(loginfo, message);
					return;
				}
				
				Car car = loadCar();
				car.setUserId(user.getId());
				OperatorReason operatioReason = Car.check(car);
				if (!operatioReason.isSuccess()) {
					String loginfo = "car input failed, reason=" + operatioReason.getReason().toString();
					String message = "用户车辆信息配置不正确,车牌号和描述信息至少需要1个不为空!";
					showInfoAndMessage(loginfo, message);
					return;
				}
				Pattern pat = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
				String weightStr = weightTextField.getText();
				if (!pat.matcher(weightStr).matches()) {
					String loginfo = "weight input format failed.";
					String message = "重量配置不正确，无法正确转换为浮点数!";
					showInfoAndMessage(loginfo, message);
					return;
				}
				double weightDouble = Double.parseDouble(weightStr);
				operatioReason = grainRequestService.addUser(user);
				if (!operatioReason.isSuccess()) {
					String loginfo = "add user info failed" + operatioReason.getReason().toString();
					String message = "用户信息插入失败,请检查返回异常和日志." + operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, message);
					return;
				}
				
				operatioReason = grainRequestService.addCar(car);
				if (!operatioReason.isSuccess()) {
					String loginfo = "add car info failed" + operatioReason.getReason().toString();
					String message = "车辆信息插入失败,请检查返回异常和日志." + operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, message);
					return;
				}
				long carId = Long.parseLong(operatioReason.getReason().toString());
				
				Order order = new Order();
				order.setTotalWeight(weightDouble);
				order.setOwner(user.getId());
				order.setCarId(carId);
				operatioReason = Order.checkStep1(order);
				if (!operatioReason.isSuccess()) {
					String loginfo = "order info check failed" + operatioReason.getReason().toString();
					String message = "订单信息不正确,请检查所属用户、车辆信息和重量." + operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, message);
					return;	
				}
				operatioReason = grainRequestService.addOrderStep1(order);
				if (!operatioReason.isSuccess()) {
					String loginfo = "order info insert failed" + operatioReason.getReason().toString();
					String message = "订单信息插入失败,请检查所属用户、车辆信息和重量." + operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, message);
					return;	
				}
				long orderId = Long.parseLong(operatioReason.getReason().toString());
				String loginfo = "order info insert success, id=" + orderId;
				log.info(loginfo);
				String message = "订单插入成功!订单ID号为:" + orderId;
				textShowInfoPane.setText(message);
				btnNewOrderButton.setEnabled(false);
			}
		});
		panel_17.add(btnNewOrderButton);
		
		Component horizontalGlue_3 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_3);
		
		Component verticalGlue_2 = Box.createVerticalGlue();
		textShowPanel.add(verticalGlue_2);
		
		Component verticalGlue_3 = Box.createVerticalGlue();
		textShowPanel.add(verticalGlue_3);
		
//		textShowPanel.add(textShowInfoPane);
	}
	
	private void showInfoAndMessage(String loginfo, String message) {
		log.warn(loginfo);
		textShowInfoPane.setText(message);
		JOptionPane.showMessageDialog(Step1MainDlg.this,message, "标题",JOptionPane.WARNING_MESSAGE); 
	}
	
	protected User loadUser() {
		try {
			User user = new User();
			// support user
			user.setId(idNumTextField.getText());
			user.setName(idNameTextField.getText());
			String birthDayStr = birthTextField.getText();
			user.setBirthday(StringUtils.isEmpty(birthDayStr) ? null : new java.sql.Date(SIMPLE_DATE_FORMAT.parse(birthDayStr).getTime()));
			user.setNation(idNationtextField.getText());
			if (rdbtnFemaleRadioButton.isSelected()) {
				user.setSex(Sex.FEMALE);
			} else {
				user.setSex(Sex.MALE);
			}
			return user;
		} catch (Exception exp) {
			exp.printStackTrace();
			log.error("get user failed " + exp.toString());
			return null;
		}
	}
	
	protected Car loadCar() {
		// support car
		Car car = new Car();
		car.setCardNum(carNumTextField.getText());
		Object carType = carTypecomboBox.getSelectedItem();
		car.setCarType(carType == null ? null : carType.toString());
		car.setCarColor(carColorTextField.getText());
		car.setCarDesc(editorPane.getText());
		return car;
	}
	
	protected void clearAllIputComponent() {
		// clear user
		idNumTextField.setText("");
		idNameTextField.setText("");
		birthTextField.setText("");
		idNationtextField.setText("");
		rdbtnSexMaleRadioButton.setSelected(true);
		// clear car
		carNumTextField.setText("");
		carTypecomboBox.setSelectedIndex(-1);
		carColorTextField.setText("");
		editorPane.setText("");
		
		// clear weight
		weightTextField.setText("");
		textShowInfoPane.setText("提示信息。");
	}
	
	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private JComboBox carTypecomboBox;
	private JTextField carColorTextField;
	private JEditorPane editorPane;
	private JButton btnNewOrderButton;
	protected void updateBirthDay() {
		String curIdNum = idNumTextField.getText();
		if (curIdNum != null && curIdNum.length() >= 14) {
			Pattern pat = Pattern.compile("[0-9]{8}");
			String birthDay = curIdNum.substring(6, 14);
			if (pat.matcher(birthDay).matches()) {
				Date birthDate;
				try {
					birthDate = SIMPLE_DATE_FORMAT.parse(birthDay.substring(0,4) + "-"
							+ birthDay.substring(4, 6) + "-"
							+ birthDay.substring(6, 8)
							);
					birthTextField.setText(SIMPLE_DATE_FORMAT.format(birthDate));
					return;
				} catch (ParseException e) {
					log.error("parse birth day failed");
					e.printStackTrace();
				}
			}
		}
		birthTextField.setText("");
	}

	protected void updateUserInfo(User user) {
		if (user != null) {
			idNumTextField.setText(user.getId());
			idNameTextField.setText(user.getName());
			birthTextField.setText(user.getBirthday() == null ? "" : SIMPLE_DATE_FORMAT.format(user.getBirthday()));
			idNationtextField.setText(user.getNation() == null? "" : user.getNation());
			Sex sex = user.getSex();
			if (sex != null && sex == Sex.FEMALE) {
				rdbtnFemaleRadioButton.setSelected(true);
			} else {
				rdbtnSexMaleRadioButton.setSelected(true);
			}
		}
	}

	protected JTextPane getTextShowInfoPane() {
		return textShowInfoPane;
	}
	protected JRadioButton getRdbtnSexMaleRadioButton() {
		return rdbtnSexMaleRadioButton;
	}
	protected JRadioButton getRdbtnFemaleRadioButton() {
		return rdbtnFemaleRadioButton;
	}
}
