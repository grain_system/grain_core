package org.grain.view;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;

import lombok.extern.slf4j.Slf4j;

import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTable;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.Dialog;

import javax.swing.Box;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@Slf4j
public class PopupComboxDlg extends JDialog {
	private JTable showDataTable;
	private boolean isSelect = false;
	private Object selectObj = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupComboxDlg dialog = new PopupComboxDlg();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public PopupComboxDlg() {
		this(null, false, Collections.emptyList(), null);
	}
	
	public boolean isSelect() {
		return isSelect;
	}
	
	public Object getSelectObj() {
		return selectObj;
	}

	/**
	 * Create the dialog.
	 */
	public PopupComboxDlg(Dialog parent, boolean modal, List<?> rows, RowHelper helper) {
		super(parent, "", modal);
		setTitle("选择框");
		setBounds(100, 100, 850, 425);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(255, 0, 0)), "\u8BF7\u9009\u62E9\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {900, 0};
		gbl_panel.rowHeights = new int[] {120, 120, 30, 30, 30};
		gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panel.setLayout(gbl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.CYAN));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.gridheight = 4;
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		panel.add(panel_1, gbc_panel_1);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		showDataTable = new JTable();
		showDataTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() >= 2) {
					log.info("double click in popup combox.");
					int select = showDataTable.getSelectedRow();
					if (select == -1) {
						JOptionPane.showMessageDialog(PopupComboxDlg.this, "必须选择一行或选择放弃!", "标题",JOptionPane.WARNING_MESSAGE); 
						return;
					}
					isSelect = true;
					selectObj = rows.get(select);
					dispose();;
				}
			}
		});
		showDataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		initTable(rows, helper);
		JScrollPane panel_4 = new JScrollPane(showDataTable);
		panel_1.add(panel_4);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.BLUE));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 4;
		panel.add(panel_2, gbc_panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);
		panel_3.setLayout(new GridLayout(1, 2, 0, 0));
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_3.add(horizontalGlue_1);
		
		JButton btnUnselect = new JButton("放弃");
		btnUnselect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.debug("click gave up in popup combox!");
				isSelect = false;
				selectObj = null;
				dispose();
			}
		});
		panel_3.add(btnUnselect);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel_3.add(horizontalGlue);
		
		JButton btnSelect = new JButton("确认选择");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int select = showDataTable.getSelectedRow();
				if (select == -1) {
					JOptionPane.showMessageDialog(PopupComboxDlg.this, "必须选择一行或选择放弃!", "标题",JOptionPane.WARNING_MESSAGE); 
					return;
				}
				isSelect = true;
				selectObj = rows.get(select);
				dispose();
			}
		});
		panel_3.add(btnSelect);
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_3.add(horizontalGlue_2);
	}
	
	protected void initTable(List<?> rows, RowHelper helper) {
		PopupTableModel ptm = new PopupTableModel(rows, helper);

		showDataTable.setModel(ptm);
		TableColumnModel tcm = showDataTable.getColumnModel();
		for (int i = 0 ; i < ptm.getColumnCount(); i ++) {
			tcm.getColumn(i).setPreferredWidth(ptm.getColumnWeight(i));
		}
	}

}
