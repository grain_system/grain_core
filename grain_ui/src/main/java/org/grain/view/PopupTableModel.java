package org.grain.view;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.swing.table.AbstractTableModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PopupTableModel extends AbstractTableModel {
	protected List<?> rows;
	protected RowHelper rowHelper;
	
	public PopupTableModel(List<?> rows, RowHelper helper) {
		this.rows = rows;
		this.rowHelper = helper;
	}
	

	public int getColumnWeight(int column) {
		return rowHelper.getTitlesWidth(column);
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return rows.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return rowHelper.getTitles().size();
	}
	
	@Override
	public String getColumnName(int column) {
		return rowHelper.getTitles().get(column);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return rowHelper.getColumn(rows.get(rowIndex), columnIndex);
	}
}
