package org.grain.view.print;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.Timestamp;
import java.util.Calendar;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.LineBorder;

import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.api.impl.StandardImpl;
import org.grain.common.constant.Constant;
import org.grain.common.constant.NumberConvert;
import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;

import lombok.extern.slf4j.Slf4j;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Dimension;
import javax.swing.JTextField;

@Slf4j
public class PrintPanel extends JPanel {
	public static final int ROW_BASE  = 35;
	public static final int ROW_COUNT = 15;
	public static final int COLUMN_BASE  = 35;
	public static final int COLUMN_COUNT = 22;
	private static StandardApi standardApi = new StandardImpl();	
	private JLabel lblUserNameInput;
	private JLabel lblUserOrderInput;
	private JLabel lblLevelInput;
	private JLabel lblBulkDensityInput;
	private JLabel lblMoistureInput;
	private JLabel lblMoisturePercentInput;
	private JLabel lblImpurityInput;
	private JLabel lblImpurityPercentInput;
	private JLabel lblIncompleteGrainInput;
	private JLabel lblIncompleteGrainPercentInput;
	private JLabel lblMildewRateInput;
	private JLabel lblMildewRatePercentInput;
	private JLabel lblTotalWeight;
	private JLabel lblNoCarWeight;
	private JLabel lblOtherWeight;
	private JLabel lblMoistureDec;
	private JLabel lblImpurityDec;
	private JLabel lblIncompleteGrainDec;
	private JLabel lblOtherDec;
	private JLabel lblTotalDecWeight;
	private JLabel lblMoistureInc;
	private JLabel lblImpurityInc;
	private JLabel lblPureWeight;
	private JLabel lblPureWeightChinese;
	private JLabel lblCarInfo;
	private JLabel lblOrderYear;
	private JLabel lblOrderMonth;
	private JLabel lblOrderDay;
	private JTextField textFieldUserLocation;
	
	public void updateOrder(Order order, User user, String carInfo) {
		try {
			if (order == null) {
				log.error("order is null");
				return;
			}
			if (user != null) {
				lblUserNameInput.setText(user.getName());
				textFieldUserLocation.setText(user.getAddress());
			}
			lblCarInfo.setText(carInfo);
			updateOrderInfo(order);
		} catch (Exception exp) {
			log.error("update order failed, err=" + exp.toString());
			JOptionPane.showMessageDialog(PrintPanel.this, "订单信息不正确!" + exp.toString(), "标题",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
	}
	
	private void updateOrderInfo(Order order) {
		updateCreateTime(order.getDoQualityTime());
		lblUserOrderInput.setText("" + order.getId());
		int level = standardApi.getLevel(order.getBulkDensity());
		order.updatePureWeight(standardApi);
		
		lblLevelInput.setText("" + (level + 1) + " 等");
		lblBulkDensityInput.setText("" + order.getBulkDensity() + " g/L");
		
		lblMoistureInput.setText("" + order.getMoisture() + " %");
		double moisturePercent = order.getMoistureWeightPer();
		lblMoisturePercentInput.setText("" + moisturePercent + " %");
		
		lblImpurityInput.setText("" + order.getImpurity() + " %");
		double impurityInput = order.getImpurityWeightPer();
		lblImpurityPercentInput.setText("" + impurityInput + " %");
		
		lblIncompleteGrainInput.setText("" + order.getIncompleteGrain() + " %");
		double incompleteGrain = order.getIncompleteGrainWeightPer();
		lblIncompleteGrainPercentInput.setText("" + incompleteGrain + " %");
		
		lblMildewRateInput.setText("" + order.getMildewRate() + " %");
		double mildewRate = - order.getMildewRate();
		lblMildewRatePercentInput.setText("" + mildewRate + " %");
		
		
		lblTotalWeight.setText("" + order.getTotalWeight());
		lblNoCarWeight.setText("" + order.getNoCarWeight());
		lblOtherWeight.setText("" + order.getAdditionWeight());
		
		double subTotalWeight = order.getTotalWeight() - order.getNoCarWeight() - order.getAdditionWeight();
		
		double calcMoistureWeight = moisturePercent * subTotalWeight / 100.0;
		if (moisturePercent <= 0.0) {
			lblMoistureDec.setText("" + (-calcMoistureWeight));
		} else {
			lblMoistureInc.setText("" + calcMoistureWeight);
		}
		
		double calcImpurityWeight = impurityInput * subTotalWeight / 100.0;
		if (impurityInput <= 0.0) {
			lblImpurityDec.setText("" + (-calcImpurityWeight));
		} else {
			lblImpurityInc.setText("" + calcImpurityWeight);
		}
		
		double calcIncompleteGrainWeight = incompleteGrain * subTotalWeight / 100.0;
		lblIncompleteGrainDec.setText("" + (-calcIncompleteGrainWeight));
		
		if (!Constant.equal(mildewRate, 0.0)) {
			lblOtherDec.setText("" + (-subTotalWeight));
		}
		
		lblTotalDecWeight.setText("" + order.getDescTotalWeight());
		
		double pureWeight = subTotalWeight - order.getDescTotalWeight() + order.getIncrTotalWeight();
		lblPureWeight.setText("" + (pureWeight));
		lblPureWeightChinese.setText(NumberConvert.convert(pureWeight));
	}
	private void updateCreateTime(Timestamp createTime) {
		Calendar cl = Calendar.getInstance();
		cl.setTimeInMillis(createTime.getTime());
		lblOrderYear.setText("" + cl.get(Calendar.YEAR));
		lblOrderMonth.setText("" + (cl.get(Calendar.MONTH) + 1));
		lblOrderDay.setText("" + cl.get(Calendar.DAY_OF_MONTH));
	}
	/**
	 * Create the panel.
	 */
	public PrintPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gridBagLayout.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gridBagLayout.rowWeights = PanelTools.getArray(1.0, ROW_COUNT);
		gridBagLayout.rowHeights = PanelTools.getArray(ROW_BASE, ROW_COUNT);
		setLayout(gridBagLayout);
		setPreferredSize(new Dimension(COLUMN_BASE * COLUMN_COUNT + COLUMN_COUNT * 2, ROW_BASE * ROW_COUNT + ROW_COUNT * 10));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.RED));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 22;
		gbc_panel.gridheight = 2;
		gbc_panel.insets = new Insets(0, 0, 0, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(2, 0, 0, 2));
		
		JLabel lblNewLabel = new JLabel("粮食描述订单");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 3, 2, 0));
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5);
		panel_5.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_4 = new JLabel("库房编号");
		panel_5.add(lblNewLabel_4);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.setLayout(new GridLayout(0, 6, 0, 0));
		
		lblOrderYear = new JLabel("");
		lblOrderYear.setHorizontalAlignment(SwingConstants.CENTER);
		panel_6.add(lblOrderYear);
		
		JLabel lblNewLabel_5 = new JLabel("年");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		panel_6.add(lblNewLabel_5);
		
		lblOrderMonth = new JLabel("");
		lblOrderMonth.setHorizontalAlignment(SwingConstants.CENTER);
		panel_6.add(lblOrderMonth);
		
		JLabel lblNewLabel_6 = new JLabel("月");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);
		panel_6.add(lblNewLabel_6);
		
		lblOrderDay = new JLabel();
		lblOrderDay.setHorizontalAlignment(SwingConstants.CENTER);
		panel_6.add(lblOrderDay);
		
		JLabel lblNewLabel_7 = new JLabel("日");
		panel_6.add(lblNewLabel_7);
		
		JPanel panel_7 = new JPanel();
		panel_4.add(panel_7);
		panel_7.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_8 = new JLabel("票号");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.CENTER);
		panel_7.add(lblNewLabel_8);
		
		Component horizontalGlue_3 = Box.createHorizontalGlue();
		panel_7.add(horizontalGlue_3);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.ORANGE));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.weighty = 1.0;
		gbc_panel_1.weightx = 1.0;
		gbc_panel_1.gridwidth = 22;
		gbc_panel_1.gridheight = 5;
		gbc_panel_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		add(panel_1, gbc_panel_1);
		panel_1.setLayout(new GridLayout(5, 0, 0, 0));
		
		JPanel panel_8 = new JPanel();
		
		GridBagLayout gbc_panel_8 = new GridBagLayout();
		gbc_panel_8.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbc_panel_8.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbc_panel_8.rowHeights = new int[] {ROW_BASE};
		gbc_panel_8.rowWeights = new double[]{1.0};
		panel_8.setLayout(gbc_panel_8);
		panel_1.add(panel_8);
		
		JPanel panel_line_usr = new JPanel();
		panel_line_usr.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_line_usr = new GridBagConstraints();
		gbc_panel_line_usr.gridwidth = 8;
		gbc_panel_line_usr.fill = GridBagConstraints.BOTH;
		gbc_panel_line_usr.gridx = 0;
		gbc_panel_line_usr.gridy = 0;
		panel_8.add(panel_line_usr, gbc_panel_line_usr);
		GridBagLayout gbl_panel_line_usr = new GridBagLayout();
		gbl_panel_line_usr.columnWidths = PanelTools.getArray(COLUMN_BASE, 8);
		gbl_panel_line_usr.rowHeights = new int[] {ROW_BASE};
		gbl_panel_line_usr.columnWeights = PanelTools.getArray(1.0, 8);
		gbl_panel_line_usr.rowWeights = new double[]{1.0};
		panel_line_usr.setLayout(gbl_panel_line_usr);
		
		JLabel lblUsrLabel = new JLabel("姓  名:");
		GridBagConstraints gbc_lblUsrLabel = new GridBagConstraints();
		gbc_lblUsrLabel.fill = GridBagConstraints.BOTH;
		gbc_lblUsrLabel.insets = new Insets(0, 0, 0, 0);
		gbc_lblUsrLabel.gridwidth = 2;
		gbc_lblUsrLabel.gridx = 0;
		gbc_lblUsrLabel.gridy = 0;
		panel_line_usr.add(lblUsrLabel, gbc_lblUsrLabel);
		
		lblUserNameInput = new JLabel();
		lblUserNameInput.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblUserNameInput = new GridBagConstraints();
		gbc_lblUserNameInput.weightx = 1.0;
		gbc_lblUserNameInput.gridwidth = 6;
		gbc_lblUserNameInput.insets = new Insets(0, 0, 0, 0);
		gbc_lblUserNameInput.gridx = 2;
		gbc_lblUserNameInput.gridy = 0;
		panel_line_usr.add(lblUserNameInput, gbc_lblUserNameInput);
		
		JPanel panel_usr_location = new JPanel();
		panel_usr_location.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_usr_location = new GridBagConstraints();
		gbc_panel_usr_location.gridwidth = 10;
		gbc_panel_usr_location.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_location.gridx = 8;
		gbc_panel_usr_location.gridy = 0;
		panel_8.add(panel_usr_location, gbc_panel_usr_location);
		GridBagLayout gbl_panel_usr_location = new GridBagLayout();
		gbl_panel_usr_location.columnWidths = PanelTools.getArray(COLUMN_BASE, 10);
		gbl_panel_usr_location.columnWeights = PanelTools.getArray(1.0, 10);
		gbl_panel_usr_location.rowHeights = new int[]{ROW_BASE};
		gbl_panel_usr_location.rowWeights = new double[]{1.0};
		panel_usr_location.setLayout(gbl_panel_usr_location);
		
		JPanel panel_usr_location_lbl = new JPanel();
		GridBagConstraints gbc_panel_usr_location_lbl = new GridBagConstraints();
		gbc_panel_usr_location_lbl.gridwidth = 2;
		gbc_panel_usr_location_lbl.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_location_lbl.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_usr_location_lbl.insets = new Insets(0, 0, 0, 0);
		gbc_panel_usr_location_lbl.gridx = 0;
		gbc_panel_usr_location_lbl.gridy = 0;
		panel_usr_location.add(panel_usr_location_lbl, gbc_panel_usr_location_lbl);
		panel_usr_location_lbl.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_usr_location = new JLabel("单位:");
		panel_usr_location_lbl.add(lblNewLabel_usr_location);
		
		JPanel panel_usr_location_input = new JPanel();
		panel_usr_location_input.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_usr_location_input = new GridBagConstraints();
		gbc_panel_usr_location_input.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_location_input.gridwidth = 8;
		gbc_panel_usr_location_input.gridx = 2;
		gbc_panel_usr_location_input.gridy = 0;
		panel_usr_location.add(panel_usr_location_input, gbc_panel_usr_location_input);
		panel_usr_location_input.setLayout(new GridLayout(0, 1, 0, 0));
		
		textFieldUserLocation = new JTextField();
		textFieldUserLocation.setEditable(false);
		textFieldUserLocation.setHorizontalAlignment(SwingConstants.LEFT);
		panel_usr_location_input.add(textFieldUserLocation);
		textFieldUserLocation.setColumns(10);
		
		JPanel panel_usr_ordere = new JPanel();
		panel_usr_ordere.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_usr_ordere = new GridBagConstraints();
		gbc_panel_usr_ordere.gridwidth = 4;
		gbc_panel_usr_ordere.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_ordere.gridx = 18;
		gbc_panel_usr_ordere.gridy = 0;
		panel_8.add(panel_usr_ordere, gbc_panel_usr_ordere);
		
		GridBagLayout gbl_panel_usr_ordere = new GridBagLayout();
		gbl_panel_usr_ordere.columnWidths = PanelTools.getArray(COLUMN_BASE * 2, 2);
		gbl_panel_usr_ordere.rowHeights = new int[]{ROW_BASE};
//		gbl_panel_5.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_usr_ordere.rowWeights = new double[]{1.0};
		panel_usr_ordere.setLayout(gbl_panel_usr_ordere);
		
		JLabel lblNewLabel_usr_order_lbl = new JLabel("订单号:");
		GridBagConstraints gbc_lblNewLabel_usr_order_lbl = new GridBagConstraints();
		gbc_lblNewLabel_usr_order_lbl.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_usr_order_lbl.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel_usr_order_lbl.gridx = 0;
		gbc_lblNewLabel_usr_order_lbl.gridy = 0;
		panel_usr_ordere.add(lblNewLabel_usr_order_lbl, gbc_lblNewLabel_usr_order_lbl);
		
		lblUserOrderInput = new JLabel();
		lblUserOrderInput.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblUserOrderInput = new GridBagConstraints();
		gbc_lblUserOrderInput.fill = GridBagConstraints.BOTH;
		gbc_lblUserOrderInput.gridx = 1;
		gbc_lblUserOrderInput.gridy = 0;
		panel_usr_ordere.add(lblUserOrderInput, gbc_lblUserOrderInput);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_9);
		GridBagLayout gbl_panel_9 = new GridBagLayout();
		gbl_panel_9.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_9.rowHeights = new int[]{ROW_BASE};
		gbl_panel_9.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_9.rowWeights = new double[] {1.0};
		panel_9.setLayout(gbl_panel_9);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_13 = new GridBagConstraints();
		gbc_panel_13.gridwidth = 3;
		gbc_panel_13.fill = GridBagConstraints.BOTH;
		gbc_panel_13.gridx = 0;
		gbc_panel_13.gridy = 0;
		panel_9.add(panel_13, gbc_panel_13);
		panel_13.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("品  种:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_13.add(lblNewLabel_1);
		
		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_14 = new GridBagConstraints();
		gbc_panel_14.gridwidth = 3;
		gbc_panel_14.insets = new Insets(0, 0, 0, 0);
		gbc_panel_14.fill = GridBagConstraints.BOTH;
		gbc_panel_14.gridx = 3;
		gbc_panel_14.gridy = 0;
		panel_9.add(panel_14, gbc_panel_14);
		
		JPanel panel_15 = new JPanel();
		panel_15.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_15 = new GridBagConstraints();
		gbc_panel_15.gridwidth = 2;
		gbc_panel_15.insets = new Insets(0, 0, 0, 0);
		gbc_panel_15.fill = GridBagConstraints.BOTH;
		gbc_panel_15.gridx = 6;
		gbc_panel_15.gridy = 0;
		panel_9.add(panel_15, gbc_panel_15);
		panel_15.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_9 = new JLabel("等  级:");
		lblNewLabel_9.setHorizontalAlignment(SwingConstants.CENTER);
		panel_15.add(lblNewLabel_9);
		
		JPanel panel_16 = new JPanel();
		panel_16.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_16 = new GridBagConstraints();
		gbc_panel_16.gridwidth = 2;
		gbc_panel_16.insets = new Insets(0, 0, 0, 0);
		gbc_panel_16.fill = GridBagConstraints.BOTH;
		gbc_panel_16.gridx = 8;
		gbc_panel_16.gridy = 0;
		panel_9.add(panel_16, gbc_panel_16);
		panel_16.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblLevelInput = new JLabel("等");
		lblLevelInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_16.add(lblLevelInput);
		
		JPanel panel_17 = new JPanel();
		panel_17.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_17 = new GridBagConstraints();
		gbc_panel_17.gridwidth = 2;
		gbc_panel_17.insets = new Insets(0, 0, 0, 0);
		gbc_panel_17.fill = GridBagConstraints.BOTH;
		gbc_panel_17.gridx = 10;
		gbc_panel_17.gridy = 0;
		panel_9.add(panel_17, gbc_panel_17);
		panel_17.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_10 = new JLabel("容重:");
		lblNewLabel_10.setHorizontalAlignment(SwingConstants.CENTER);
		panel_17.add(lblNewLabel_10);
		
		JPanel panel_18 = new JPanel();
		panel_18.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_18 = new GridBagConstraints();
		gbc_panel_18.gridwidth = 3;
		gbc_panel_18.insets = new Insets(0, 0, 0, 0);
		gbc_panel_18.fill = GridBagConstraints.BOTH;
		gbc_panel_18.gridx = 12;
		gbc_panel_18.gridy = 0;
		panel_9.add(panel_18, gbc_panel_18);
		panel_18.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblBulkDensityInput = new JLabel("g/L");
		lblBulkDensityInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_18.add(lblBulkDensityInput);
		
		JPanel panel_19 = new JPanel();
		panel_19.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_19 = new GridBagConstraints();
		gbc_panel_19.gridwidth = 3;
		gbc_panel_19.insets = new Insets(0, 0, 0, 0);
		gbc_panel_19.fill = GridBagConstraints.BOTH;
		gbc_panel_19.gridx = 15;
		gbc_panel_19.gridy = 0;
		panel_9.add(panel_19, gbc_panel_19);
		panel_19.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_12 = new JLabel("检查袋数:");
		lblNewLabel_12.setHorizontalAlignment(SwingConstants.CENTER);
		panel_19.add(lblNewLabel_12);
		
		JPanel panel_20 = new JPanel();
		panel_20.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_20 = new GridBagConstraints();
		gbc_panel_20.gridwidth = 4;
		gbc_panel_20.fill = GridBagConstraints.BOTH;
		gbc_panel_20.gridx = 18;
		gbc_panel_20.gridy = 0;
		panel_9.add(panel_20, gbc_panel_20);
		panel_20.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_13 = new JLabel("个袋");
		lblNewLabel_13.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_20.add(lblNewLabel_13);
		
		JPanel panel_30 = new JPanel();
		panel_30.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_30);
		GridBagLayout gbl_panel_30 = new GridBagLayout();
		gbl_panel_30.columnWidths = new int[]{35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 0};
		gbl_panel_30.rowHeights = new int[]{35, 0};
		gbl_panel_30.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_30.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_30.setLayout(gbl_panel_30);
		
		JPanel panel_31 = new JPanel();
		panel_31.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_31 = new GridBagConstraints();
		gbc_panel_31.fill = GridBagConstraints.BOTH;
		gbc_panel_31.gridwidth = 3;
		gbc_panel_31.insets = new Insets(0, 0, 0, 0);
		gbc_panel_31.gridx = 0;
		gbc_panel_31.gridy = 0;
		panel_30.add(panel_31, gbc_panel_31);
		panel_31.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_1_1 = new JLabel("水  份:");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_31.add(lblNewLabel_1_1);
		
		JPanel panel_32 = new JPanel();
		panel_32.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_32 = new GridBagConstraints();
		gbc_panel_32.fill = GridBagConstraints.BOTH;
		gbc_panel_32.gridwidth = 3;
		gbc_panel_32.insets = new Insets(0, 0, 0, 0);
		gbc_panel_32.gridx = 3;
		gbc_panel_32.gridy = 0;
		panel_30.add(panel_32, gbc_panel_32);
		panel_32.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMoistureInput = new JLabel("%");
		lblMoistureInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_32.add(lblMoistureInput);
		
		JPanel panel_33 = new JPanel();
		panel_33.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_33 = new GridBagConstraints();
		gbc_panel_33.fill = GridBagConstraints.BOTH;
		gbc_panel_33.gridwidth = 2;
		gbc_panel_33.insets = new Insets(0, 0, 0, 0);
		gbc_panel_33.gridx = 6;
		gbc_panel_33.gridy = 0;
		panel_30.add(panel_33, gbc_panel_33);
		panel_33.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_9_1 = new JLabel("增减量:");
		lblNewLabel_9_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_33.add(lblNewLabel_9_1);
		
		JPanel panel_34 = new JPanel();
		panel_34.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_34 = new GridBagConstraints();
		gbc_panel_34.fill = GridBagConstraints.BOTH;
		gbc_panel_34.gridwidth = 2;
		gbc_panel_34.insets = new Insets(0, 0, 0, 0);
		gbc_panel_34.gridx = 8;
		gbc_panel_34.gridy = 0;
		panel_30.add(panel_34, gbc_panel_34);
		panel_34.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMoisturePercentInput = new JLabel("%");
		lblMoisturePercentInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_34.add(lblMoisturePercentInput);
		
		JPanel panel_35 = new JPanel();
		panel_35.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_35 = new GridBagConstraints();
		gbc_panel_35.fill = GridBagConstraints.BOTH;
		gbc_panel_35.gridwidth = 2;
		gbc_panel_35.insets = new Insets(0, 0, 0, 0);
		gbc_panel_35.gridx = 10;
		gbc_panel_35.gridy = 0;
		panel_30.add(panel_35, gbc_panel_35);
		panel_35.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_10_1 = new JLabel("杂质:");
		lblNewLabel_10_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_35.add(lblNewLabel_10_1);
		
		JPanel panel_36 = new JPanel();
		panel_36.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_36 = new GridBagConstraints();
		gbc_panel_36.fill = GridBagConstraints.BOTH;
		gbc_panel_36.gridwidth = 3;
		gbc_panel_36.insets = new Insets(0, 0, 0, 0);
		gbc_panel_36.gridx = 12;
		gbc_panel_36.gridy = 0;
		panel_30.add(panel_36, gbc_panel_36);
		panel_36.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblImpurityInput = new JLabel("%");
		lblImpurityInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_36.add(lblImpurityInput);
		
		JPanel panel_37 = new JPanel();
		panel_37.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_37 = new GridBagConstraints();
		gbc_panel_37.fill = GridBagConstraints.BOTH;
		gbc_panel_37.gridwidth = 3;
		gbc_panel_37.insets = new Insets(0, 0, 0, 0);
		gbc_panel_37.gridx = 15;
		gbc_panel_37.gridy = 0;
		panel_30.add(panel_37, gbc_panel_37);
		panel_37.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_12_1 = new JLabel("增减量:");
		lblNewLabel_12_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_37.add(lblNewLabel_12_1);
		
		JPanel panel_38 = new JPanel();
		panel_38.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_38 = new GridBagConstraints();
		gbc_panel_38.fill = GridBagConstraints.BOTH;
		gbc_panel_38.gridwidth = 4;
		gbc_panel_38.gridx = 18;
		gbc_panel_38.gridy = 0;
		panel_30.add(panel_38, gbc_panel_38);
		panel_38.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblImpurityPercentInput = new JLabel("%");
		lblImpurityPercentInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_38.add(lblImpurityPercentInput);
		
		JPanel panel_40 = new JPanel();
		panel_40.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_40);
		GridBagLayout gbl_panel_40 = new GridBagLayout();
		gbl_panel_40.columnWidths = new int[]{35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 0};
		gbl_panel_40.rowHeights = new int[]{35, 0};
		gbl_panel_40.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_40.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_40.setLayout(gbl_panel_40);
		
		JPanel panel_41 = new JPanel();
		panel_41.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_41 = new GridBagConstraints();
		gbc_panel_41.fill = GridBagConstraints.BOTH;
		gbc_panel_41.gridwidth = 3;
		gbc_panel_41.insets = new Insets(0, 0, 0, 0);
		gbc_panel_41.gridx = 0;
		gbc_panel_41.gridy = 0;
		panel_40.add(panel_41, gbc_panel_41);
		panel_41.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_1_2 = new JLabel("不完善粒:");
		lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_41.add(lblNewLabel_1_2);
		
		JPanel panel_42 = new JPanel();
		panel_42.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_42 = new GridBagConstraints();
		gbc_panel_42.fill = GridBagConstraints.BOTH;
		gbc_panel_42.gridwidth = 3;
		gbc_panel_42.insets = new Insets(0, 0, 0, 0);
		gbc_panel_42.gridx = 3;
		gbc_panel_42.gridy = 0;
		panel_40.add(panel_42, gbc_panel_42);
		panel_42.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblIncompleteGrainInput = new JLabel("%");
		lblIncompleteGrainInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_42.add(lblIncompleteGrainInput);
		
		JPanel panel_43 = new JPanel();
		panel_43.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_43 = new GridBagConstraints();
		gbc_panel_43.fill = GridBagConstraints.BOTH;
		gbc_panel_43.gridwidth = 2;
		gbc_panel_43.insets = new Insets(0, 0, 0, 0);
		gbc_panel_43.gridx = 6;
		gbc_panel_43.gridy = 0;
		panel_40.add(panel_43, gbc_panel_43);
		panel_43.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_9_2 = new JLabel("减  量:");
		lblNewLabel_9_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_43.add(lblNewLabel_9_2);
		
		JPanel panel_44 = new JPanel();
		panel_44.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_44 = new GridBagConstraints();
		gbc_panel_44.fill = GridBagConstraints.BOTH;
		gbc_panel_44.gridwidth = 2;
		gbc_panel_44.insets = new Insets(0, 0, 0, 0);
		gbc_panel_44.gridx = 8;
		gbc_panel_44.gridy = 0;
		panel_40.add(panel_44, gbc_panel_44);
		panel_44.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblIncompleteGrainPercentInput = new JLabel("%");
		lblIncompleteGrainPercentInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_44.add(lblIncompleteGrainPercentInput);
		
		JPanel panel_45 = new JPanel();
		panel_45.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_45 = new GridBagConstraints();
		gbc_panel_45.fill = GridBagConstraints.BOTH;
		gbc_panel_45.gridwidth = 2;
		gbc_panel_45.insets = new Insets(0, 0, 0, 0);
		gbc_panel_45.gridx = 10;
		gbc_panel_45.gridy = 0;
		panel_40.add(panel_45, gbc_panel_45);
		panel_45.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_10_2 = new JLabel("霉变率:");
		lblNewLabel_10_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_45.add(lblNewLabel_10_2);
		
		JPanel panel_46 = new JPanel();
		panel_46.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_46 = new GridBagConstraints();
		gbc_panel_46.fill = GridBagConstraints.BOTH;
		gbc_panel_46.gridwidth = 3;
		gbc_panel_46.insets = new Insets(0, 0, 0, 0);
		gbc_panel_46.gridx = 12;
		gbc_panel_46.gridy = 0;
		panel_40.add(panel_46, gbc_panel_46);
		panel_46.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMildewRateInput = new JLabel("%");
		lblMildewRateInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_46.add(lblMildewRateInput);
		
		JPanel panel_47 = new JPanel();
		panel_47.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_47 = new GridBagConstraints();
		gbc_panel_47.fill = GridBagConstraints.BOTH;
		gbc_panel_47.gridwidth = 3;
		gbc_panel_47.insets = new Insets(0, 0, 0, 0);
		gbc_panel_47.gridx = 15;
		gbc_panel_47.gridy = 0;
		panel_40.add(panel_47, gbc_panel_47);
		panel_47.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_12_2 = new JLabel("减  量:");
		lblNewLabel_12_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_47.add(lblNewLabel_12_2);
		
		JPanel panel_48 = new JPanel();
		panel_48.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_48 = new GridBagConstraints();
		gbc_panel_48.fill = GridBagConstraints.BOTH;
		gbc_panel_48.gridwidth = 4;
		gbc_panel_48.gridx = 18;
		gbc_panel_48.gridy = 0;
		panel_40.add(panel_48, gbc_panel_48);
		panel_48.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMildewRatePercentInput = new JLabel("%");
		lblMildewRatePercentInput.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_48.add(lblMildewRatePercentInput);
		
		JPanel panel_12 = new JPanel();
		panel_12.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_12);
		GridBagLayout gbl_panel_12 = new GridBagLayout();
		gbl_panel_12.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_12.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_12.rowHeights = new int[] {35};
		gbl_panel_12.rowWeights = new double[]{1.0};
		panel_12.setLayout(gbl_panel_12);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_10 = new GridBagConstraints();
		gbc_panel_10.gridwidth = 3;
		gbc_panel_10.fill = GridBagConstraints.BOTH;
		gbc_panel_10.gridx = 0;
		gbc_panel_10.gridy = 0;
		panel_12.add(panel_10, gbc_panel_10);
		panel_10.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_17 = new JLabel("备  注:");
		lblNewLabel_17.setHorizontalAlignment(SwingConstants.CENTER);
		panel_10.add(lblNewLabel_17);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_11 = new GridBagConstraints();
		gbc_panel_11.gridwidth = 19;
		gbc_panel_11.fill = GridBagConstraints.BOTH;
		gbc_panel_11.gridx = 3;
		gbc_panel_11.gridy = 0;
		panel_12.add(panel_11, gbc_panel_11);
		panel_11.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.GREEN));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.gridwidth = 22;
		gbc_panel_2.gridheight = 6;
		gbc_panel_2.insets = new Insets(0, 0, 0, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 7;
		add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWeights = new double[] {1.0};
		gbl_panel_2.columnWidths =  new int[]{COLUMN_BASE * COLUMN_COUNT};
		gbl_panel_2.rowHeights = PanelTools.getArray(ROW_BASE, 6);
		gbl_panel_2.rowWeights = PanelTools.getArray(1.0, 6);
		panel_2.setLayout(gbl_panel_2);
		
		JPanel panel_21 = new JPanel();
		GridBagConstraints gbc_panel_21 = new GridBagConstraints();
		gbc_panel_21.gridheight = 2;
		gbc_panel_21.fill = GridBagConstraints.BOTH;
		gbc_panel_21.gridx = 0;
		gbc_panel_21.gridy = 0;
		panel_2.add(panel_21, gbc_panel_21);
		GridBagLayout gbl_panel_21 = new GridBagLayout();
		gbl_panel_21.rowHeights = new int[] {ROW_BASE, ROW_BASE};
		gbl_panel_21.rowWeights = new double[]{1.0, 1.0};
		gbl_panel_21.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_21.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		panel_21.setLayout(gbl_panel_21);
		
		JPanel panel_26 = new JPanel();
		panel_26.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_26 = new GridBagConstraints();
		gbc_panel_26.gridwidth = 4;
		gbc_panel_26.gridheight = 2;
		gbc_panel_26.fill = GridBagConstraints.BOTH;
		gbc_panel_26.gridx = 0;
		gbc_panel_26.gridy = 0;
		panel_21.add(panel_26, gbc_panel_26);
		panel_26.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_2 = new JLabel("总毛重(公斤)");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_26.add(lblNewLabel_2);
		
		JPanel panel_27 = new JPanel();
		panel_27.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_27 = new GridBagConstraints();
		gbc_panel_27.gridwidth = 18;
		gbc_panel_27.fill = GridBagConstraints.BOTH;
		gbc_panel_27.insets = new Insets(0, 0, 0, 0);
		gbc_panel_27.gridx = 4;
		gbc_panel_27.gridy = 0;
		panel_21.add(panel_27, gbc_panel_27);
		GridBagLayout gbl_panel_27 = new GridBagLayout();
		gbl_panel_27.columnWidths = PanelTools.getArray(COLUMN_BASE, 18);
		gbl_panel_27.columnWeights = PanelTools.getArray(1.0, 18);
		gbl_panel_27.rowHeights = new int[]{ROW_BASE};
		gbl_panel_27.rowWeights = new double[]{1.0};
		panel_27.setLayout(gbl_panel_27);
		
		JPanel panel_29 = new JPanel();
		panel_29.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_29 = new GridBagConstraints();
		gbc_panel_29.fill = GridBagConstraints.BOTH;
		gbc_panel_29.gridwidth = 12;
		gbc_panel_29.insets = new Insets(0, 0, 0, 0);
		gbc_panel_29.gridx = 0;
		gbc_panel_29.gridy = 0;
		panel_27.add(panel_29, gbc_panel_29);
		panel_29.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_18 = new JLabel("综合减量(公斤)");
		lblNewLabel_18.setHorizontalAlignment(SwingConstants.CENTER);
		panel_29.add(lblNewLabel_18);
		
		JPanel panel_39 = new JPanel();
		panel_39.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_39 = new GridBagConstraints();
		gbc_panel_39.fill = GridBagConstraints.BOTH;
		gbc_panel_39.gridwidth = 2;
		gbc_panel_39.insets = new Insets(0, 0, 0, 0);
		gbc_panel_39.gridx = 12;
		gbc_panel_39.gridy = 0;
		panel_27.add(panel_39, gbc_panel_39);
		panel_39.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_19 = new JLabel("增量(公斤)");
		lblNewLabel_19.setHorizontalAlignment(SwingConstants.CENTER);
		panel_39.add(lblNewLabel_19);
		
		JPanel panel_49 = new JPanel();
		panel_49.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_49 = new GridBagConstraints();
		gbc_panel_49.fill = GridBagConstraints.BOTH;
		gbc_panel_49.gridwidth = 2;
		gbc_panel_49.insets = new Insets(0, 0, 0, 0);
		gbc_panel_49.gridx = 14;
		gbc_panel_49.gridy = 0;
		panel_27.add(panel_49, gbc_panel_49);
		panel_49.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_20 = new JLabel("车号:");
		lblNewLabel_20.setHorizontalAlignment(SwingConstants.CENTER);
		panel_49.add(lblNewLabel_20);
		
		JPanel panel_50 = new JPanel();
		panel_50.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_50 = new GridBagConstraints();
		gbc_panel_50.gridwidth = 2;
		gbc_panel_50.fill = GridBagConstraints.BOTH;
		gbc_panel_50.gridx = 16;
		gbc_panel_50.gridy = 0;
		panel_27.add(panel_50, gbc_panel_50);
		panel_50.setLayout(new GridLayout(1, 0, 0, 0));
		
		lblCarInfo = new JLabel("");
		lblCarInfo.setHorizontalAlignment(SwingConstants.LEFT);
		panel_50.add(lblCarInfo);
		
		JPanel panel_28 = new JPanel();
		GridBagConstraints gbc_panel_28 = new GridBagConstraints();
		gbc_panel_28.gridwidth = 18;
		gbc_panel_28.fill = GridBagConstraints.BOTH;
		gbc_panel_28.gridx = 4;
		gbc_panel_28.gridy = 1;
		panel_21.add(panel_28, gbc_panel_28);
		GridBagLayout gbl_panel_28 = new GridBagLayout();
		gbl_panel_28.columnWidths = PanelTools.getArray(COLUMN_BASE, 18);
		gbl_panel_28.columnWeights = PanelTools.getArray(1.0, 18);
		gbl_panel_28.rowWeights = new double[]{1.0};
		gbl_panel_28.rowHeights = new int[] { ROW_BASE };
		panel_28.setLayout(gbl_panel_28);
		
		JPanel panel_51 = new JPanel();
		panel_51.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_51 = new GridBagConstraints();
		gbc_panel_51.fill = GridBagConstraints.BOTH;
		gbc_panel_51.gridwidth = 3;
		gbc_panel_51.gridx = 0;
		gbc_panel_51.gridy = 0;
		panel_28.add(panel_51, gbc_panel_51);
		panel_51.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_21 = new JLabel("车皮毛重");
		lblNewLabel_21.setHorizontalAlignment(SwingConstants.CENTER);
		panel_51.add(lblNewLabel_21);
		
		JPanel panel_52 = new JPanel();
		panel_52.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_52 = new GridBagConstraints();
		gbc_panel_52.fill = GridBagConstraints.BOTH;
		gbc_panel_52.gridwidth = 2;
		gbc_panel_52.insets = new Insets(0, 0, 0, 0);
		gbc_panel_52.gridx = 3;
		gbc_panel_52.gridy = 0;
		panel_28.add(panel_52, gbc_panel_52);
		panel_52.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_22 = new JLabel("袋皮重");
		lblNewLabel_22.setHorizontalAlignment(SwingConstants.CENTER);
		panel_52.add(lblNewLabel_22);
		
		JPanel panel_53 = new JPanel();
		panel_53.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_53 = new GridBagConstraints();
		gbc_panel_53.fill = GridBagConstraints.BOTH;
		gbc_panel_53.insets = new Insets(0, 0, 0, 0);
		gbc_panel_53.gridx = 5;
		gbc_panel_53.gridy = 0;
		panel_28.add(panel_53, gbc_panel_53);
		panel_53.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_23 = new JLabel("水份");
		lblNewLabel_23.setHorizontalAlignment(SwingConstants.CENTER);
		panel_53.add(lblNewLabel_23);
		
		JPanel panel_54 = new JPanel();
		panel_54.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_54 = new GridBagConstraints();
		gbc_panel_54.fill = GridBagConstraints.BOTH;
		gbc_panel_54.insets = new Insets(0, 0, 0, 0);
		gbc_panel_54.gridx = 6;
		gbc_panel_54.gridy = 0;
		panel_28.add(panel_54, gbc_panel_54);
		panel_54.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_24 = new JLabel("杂质");
		lblNewLabel_24.setHorizontalAlignment(SwingConstants.CENTER);
		panel_54.add(lblNewLabel_24);
		
		JPanel panel_55 = new JPanel();
		panel_55.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_55 = new GridBagConstraints();
		gbc_panel_55.gridwidth = 2;
		gbc_panel_55.fill = GridBagConstraints.BOTH;
		gbc_panel_55.insets = new Insets(0, 0, 0, 0);
		gbc_panel_55.gridx = 7;
		gbc_panel_55.gridy = 0;
		panel_28.add(panel_55, gbc_panel_55);
		panel_55.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_25 = new JLabel("不完粒");
		lblNewLabel_25.setHorizontalAlignment(SwingConstants.CENTER);
		panel_55.add(lblNewLabel_25);
		
		JPanel panel_56 = new JPanel();
		panel_56.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_56 = new GridBagConstraints();
		gbc_panel_56.fill = GridBagConstraints.BOTH;
		gbc_panel_56.insets = new Insets(0, 0, 0, 0);
		gbc_panel_56.gridx = 9;
		gbc_panel_56.gridy = 0;
		panel_28.add(panel_56, gbc_panel_56);
		panel_56.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_26 = new JLabel("其他");
		lblNewLabel_26.setHorizontalAlignment(SwingConstants.CENTER);
		panel_56.add(lblNewLabel_26);
		
		JPanel panel_57 = new JPanel();
		panel_57.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_57 = new GridBagConstraints();
		gbc_panel_57.fill = GridBagConstraints.BOTH;
		gbc_panel_57.gridwidth = 2;
		gbc_panel_57.insets = new Insets(0, 0, 0, 0);
		gbc_panel_57.gridx = 10;
		gbc_panel_57.gridy = 0;
		panel_28.add(panel_57, gbc_panel_57);
		panel_57.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_27 = new JLabel("总减量");
		lblNewLabel_27.setHorizontalAlignment(SwingConstants.CENTER);
		panel_57.add(lblNewLabel_27);
		
		JPanel panel_58 = new JPanel();
		panel_58.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_58 = new GridBagConstraints();
		gbc_panel_58.fill = GridBagConstraints.BOTH;
		gbc_panel_58.insets = new Insets(0, 0, 0, 0);
		gbc_panel_58.gridx = 12;
		gbc_panel_58.gridy = 0;
		panel_28.add(panel_58, gbc_panel_58);
		panel_58.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_28 = new JLabel("水份");
		lblNewLabel_28.setHorizontalAlignment(SwingConstants.CENTER);
		panel_58.add(lblNewLabel_28);
		
		JPanel panel_59 = new JPanel();
		panel_59.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_59 = new GridBagConstraints();
		gbc_panel_59.fill = GridBagConstraints.BOTH;
		gbc_panel_59.insets = new Insets(0, 0, 0, 0);
		gbc_panel_59.gridx = 13;
		gbc_panel_59.gridy = 0;
		panel_28.add(panel_59, gbc_panel_59);
		panel_59.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_29 = new JLabel("杂质");
		lblNewLabel_29.setHorizontalAlignment(SwingConstants.CENTER);
		panel_59.add(lblNewLabel_29);
		
		JPanel panel_60 = new JPanel();
		panel_60.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_60 = new GridBagConstraints();
		gbc_panel_60.fill = GridBagConstraints.BOTH;
		gbc_panel_60.gridwidth = 4;
		gbc_panel_60.gridx = 14;
		gbc_panel_60.gridy = 0;
		panel_28.add(panel_60, gbc_panel_60);
		panel_60.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_30 = new JLabel("净重(公斤)");
		lblNewLabel_30.setHorizontalAlignment(SwingConstants.CENTER);
		panel_60.add(lblNewLabel_30);
		
		JPanel panel_22 = new JPanel();
		GridBagConstraints gbc_panel_22 = new GridBagConstraints();
		gbc_panel_22.fill = GridBagConstraints.BOTH;
		gbc_panel_22.gridx = 0;
		gbc_panel_22.gridy = 2;
		panel_2.add(panel_22, gbc_panel_22);
		GridBagLayout gbl_panel_22 = new GridBagLayout();
		gbl_panel_22.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_22.rowHeights = new int[]{ROW_BASE};
		gbl_panel_22.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_22.rowWeights = new double[]{1.0};
		panel_22.setLayout(gbl_panel_22);
		
		JPanel panel_61 = new JPanel();
		panel_61.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_61 = new GridBagConstraints();
		gbc_panel_61.fill = GridBagConstraints.BOTH;
		gbc_panel_61.gridwidth = 4;
		gbc_panel_61.gridx = 0;
		gbc_panel_61.gridy = 0;
		panel_22.add(panel_61, gbc_panel_61);
		panel_61.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblTotalWeight = new JLabel("");
		panel_61.add(lblTotalWeight);
		
		JPanel panel_51_1 = new JPanel();
		panel_51_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_51_1 = new GridBagConstraints();
		gbc_panel_51_1.fill = GridBagConstraints.BOTH;
		gbc_panel_51_1.gridwidth = 3;
		gbc_panel_51_1.gridx = 4;
		gbc_panel_51_1.gridy = 0;
		panel_22.add(panel_51_1, gbc_panel_51_1);
		panel_51_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblNoCarWeight = new JLabel("");
		lblNoCarWeight.setHorizontalAlignment(SwingConstants.CENTER);
		panel_51_1.add(lblNoCarWeight);
		
		JPanel panel_52_1 = new JPanel();
		panel_52_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_52_1 = new GridBagConstraints();
		gbc_panel_52_1.fill = GridBagConstraints.BOTH;
		gbc_panel_52_1.gridwidth = 2;
		gbc_panel_52_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_52_1.gridx = 7;
		gbc_panel_52_1.gridy = 0;
		panel_22.add(panel_52_1, gbc_panel_52_1);
		panel_52_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblOtherWeight = new JLabel("");
		lblOtherWeight.setHorizontalAlignment(SwingConstants.CENTER);
		panel_52_1.add(lblOtherWeight);
		
		JPanel panel_53_1 = new JPanel();
		panel_53_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_53_1 = new GridBagConstraints();
		gbc_panel_53_1.fill = GridBagConstraints.BOTH;
		gbc_panel_53_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_53_1.gridx = 9;
		gbc_panel_53_1.gridy = 0;
		panel_22.add(panel_53_1, gbc_panel_53_1);
		panel_53_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMoistureDec = new JLabel("");
		lblMoistureDec.setHorizontalAlignment(SwingConstants.CENTER);
		panel_53_1.add(lblMoistureDec);
		
		JPanel panel_54_1 = new JPanel();
		panel_54_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_54_1 = new GridBagConstraints();
		gbc_panel_54_1.fill = GridBagConstraints.BOTH;
		gbc_panel_54_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_54_1.gridx = 10;
		gbc_panel_54_1.gridy = 0;
		panel_22.add(panel_54_1, gbc_panel_54_1);
		panel_54_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblImpurityDec = new JLabel("");
		lblImpurityDec.setHorizontalAlignment(SwingConstants.CENTER);
		panel_54_1.add(lblImpurityDec);
		
		JPanel panel_55_1 = new JPanel();
		panel_55_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_55_1 = new GridBagConstraints();
		gbc_panel_55_1.fill = GridBagConstraints.BOTH;
		gbc_panel_55_1.gridwidth = 2;
		gbc_panel_55_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_55_1.gridx = 11;
		gbc_panel_55_1.gridy = 0;
		panel_22.add(panel_55_1, gbc_panel_55_1);
		panel_55_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblIncompleteGrainDec = new JLabel("");
		lblIncompleteGrainDec.setHorizontalAlignment(SwingConstants.CENTER);
		panel_55_1.add(lblIncompleteGrainDec);
		
		JPanel panel_56_1 = new JPanel();
		panel_56_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_56_1 = new GridBagConstraints();
		gbc_panel_56_1.fill = GridBagConstraints.BOTH;
		gbc_panel_56_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_56_1.gridx = 13;
		gbc_panel_56_1.gridy = 0;
		panel_22.add(panel_56_1, gbc_panel_56_1);
		panel_56_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblOtherDec = new JLabel("");
		lblOtherDec.setHorizontalAlignment(SwingConstants.CENTER);
		panel_56_1.add(lblOtherDec);
		
		JPanel panel_57_1 = new JPanel();
		panel_57_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_57_1 = new GridBagConstraints();
		gbc_panel_57_1.fill = GridBagConstraints.BOTH;
		gbc_panel_57_1.gridwidth = 2;
		gbc_panel_57_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_57_1.gridx = 14;
		gbc_panel_57_1.gridy = 0;
		panel_22.add(panel_57_1, gbc_panel_57_1);
		panel_57_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblTotalDecWeight = new JLabel("");
		lblTotalDecWeight.setHorizontalAlignment(SwingConstants.CENTER);
		panel_57_1.add(lblTotalDecWeight);
		
		JPanel panel_58_1 = new JPanel();
		panel_58_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_58_1 = new GridBagConstraints();
		gbc_panel_58_1.fill = GridBagConstraints.BOTH;
		gbc_panel_58_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_58_1.gridx = 16;
		gbc_panel_58_1.gridy = 0;
		panel_22.add(panel_58_1, gbc_panel_58_1);
		panel_58_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblMoistureInc = new JLabel("");
		lblMoistureInc.setHorizontalAlignment(SwingConstants.CENTER);
		panel_58_1.add(lblMoistureInc);
		
		JPanel panel_59_1 = new JPanel();
		panel_59_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_59_1 = new GridBagConstraints();
		gbc_panel_59_1.fill = GridBagConstraints.BOTH;
		gbc_panel_59_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_59_1.gridx = 17;
		gbc_panel_59_1.gridy = 0;
		panel_22.add(panel_59_1, gbc_panel_59_1);
		panel_59_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblImpurityInc = new JLabel("");
		lblImpurityInc.setHorizontalAlignment(SwingConstants.CENTER);
		panel_59_1.add(lblImpurityInc);
		
		JPanel panel_60_1 = new JPanel();
		panel_60_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_60_1 = new GridBagConstraints();
		gbc_panel_60_1.fill = GridBagConstraints.BOTH;
		gbc_panel_60_1.gridwidth = 4;
		gbc_panel_60_1.gridx = 18;
		gbc_panel_60_1.gridy = 0;
		panel_22.add(panel_60_1, gbc_panel_60_1);
		panel_60_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblPureWeight = new JLabel("");
		lblPureWeight.setHorizontalAlignment(SwingConstants.CENTER);
		panel_60_1.add(lblPureWeight);
		
		JPanel panel_23 = new JPanel();
		GridBagConstraints gbc_panel_23 = new GridBagConstraints();
		gbc_panel_23.fill = GridBagConstraints.BOTH;
		gbc_panel_23.gridx = 0;
		gbc_panel_23.gridy = 3;
		panel_2.add(panel_23, gbc_panel_23);
		GridBagLayout gbl_panel_23 = new GridBagLayout();
		gbl_panel_23.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_23.rowHeights = new int[]{ROW_BASE};
		gbl_panel_23.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_23.rowWeights = new double[]{1.0};
		panel_23.setLayout(gbl_panel_23);
		
		JPanel panel_61_1 = new JPanel();
		panel_61_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_61_1 = new GridBagConstraints();
		gbc_panel_61_1.gridwidth = 4;
		gbc_panel_61_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_61_1.fill = GridBagConstraints.BOTH;
		gbc_panel_61_1.gridx = 0;
		gbc_panel_61_1.gridy = 0;
		panel_23.add(panel_61_1, gbc_panel_61_1);
		panel_61_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_31_1 = new JLabel("");
		panel_61_1.add(lblNewLabel_31_1);
		
		JPanel panel_51_1_1 = new JPanel();
		panel_51_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_51_1_1 = new GridBagConstraints();
		gbc_panel_51_1_1.gridwidth = 3;
		gbc_panel_51_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_51_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_51_1_1.gridx = 4;
		gbc_panel_51_1_1.gridy = 0;
		panel_23.add(panel_51_1_1, gbc_panel_51_1_1);
		panel_51_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_21_1_1 = new JLabel("");
		lblNewLabel_21_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_51_1_1.add(lblNewLabel_21_1_1);
		
		JPanel panel_52_1_1 = new JPanel();
		panel_52_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_52_1_1 = new GridBagConstraints();
		gbc_panel_52_1_1.gridwidth = 2;
		gbc_panel_52_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_52_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_52_1_1.gridx = 7;
		gbc_panel_52_1_1.gridy = 0;
		panel_23.add(panel_52_1_1, gbc_panel_52_1_1);
		panel_52_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_22_1_1 = new JLabel("");
		lblNewLabel_22_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_52_1_1.add(lblNewLabel_22_1_1);
		
		JPanel panel_53_1_1 = new JPanel();
		panel_53_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_53_1_1 = new GridBagConstraints();
		gbc_panel_53_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_53_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_53_1_1.gridx = 9;
		gbc_panel_53_1_1.gridy = 0;
		panel_23.add(panel_53_1_1, gbc_panel_53_1_1);
		panel_53_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_23_1_1 = new JLabel("");
		lblNewLabel_23_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_53_1_1.add(lblNewLabel_23_1_1);
		
		JPanel panel_54_1_1 = new JPanel();
		panel_54_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_54_1_1 = new GridBagConstraints();
		gbc_panel_54_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_54_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_54_1_1.gridx = 10;
		gbc_panel_54_1_1.gridy = 0;
		panel_23.add(panel_54_1_1, gbc_panel_54_1_1);
		panel_54_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_24_1_1 = new JLabel("");
		lblNewLabel_24_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_54_1_1.add(lblNewLabel_24_1_1);
		
		JPanel panel_55_1_1 = new JPanel();
		panel_55_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_55_1_1 = new GridBagConstraints();
		gbc_panel_55_1_1.gridwidth = 2;
		gbc_panel_55_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_55_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_55_1_1.gridx = 11;
		gbc_panel_55_1_1.gridy = 0;
		panel_23.add(panel_55_1_1, gbc_panel_55_1_1);
		panel_55_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_25_1_1 = new JLabel("");
		lblNewLabel_25_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_55_1_1.add(lblNewLabel_25_1_1);
		
		JPanel panel_56_1_1 = new JPanel();
		panel_56_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_56_1_1 = new GridBagConstraints();
		gbc_panel_56_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_56_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_56_1_1.gridx = 13;
		gbc_panel_56_1_1.gridy = 0;
		panel_23.add(panel_56_1_1, gbc_panel_56_1_1);
		panel_56_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_26_1_1 = new JLabel("");
		lblNewLabel_26_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_56_1_1.add(lblNewLabel_26_1_1);
		
		JPanel panel_57_1_1 = new JPanel();
		panel_57_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_57_1_1 = new GridBagConstraints();
		gbc_panel_57_1_1.gridwidth = 2;
		gbc_panel_57_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_57_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_57_1_1.gridx = 14;
		gbc_panel_57_1_1.gridy = 0;
		panel_23.add(panel_57_1_1, gbc_panel_57_1_1);
		panel_57_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_27_1_1 = new JLabel("");
		lblNewLabel_27_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_57_1_1.add(lblNewLabel_27_1_1);
		
		JPanel panel_58_1_1 = new JPanel();
		panel_58_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_58_1_1 = new GridBagConstraints();
		gbc_panel_58_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_58_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_58_1_1.gridx = 16;
		gbc_panel_58_1_1.gridy = 0;
		panel_23.add(panel_58_1_1, gbc_panel_58_1_1);
		panel_58_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_28_1_1 = new JLabel("");
		lblNewLabel_28_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_58_1_1.add(lblNewLabel_28_1_1);
		
		JPanel panel_59_1_1 = new JPanel();
		panel_59_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_59_1_1 = new GridBagConstraints();
		gbc_panel_59_1_1.insets = new Insets(0, 0, 0, 0);
		gbc_panel_59_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_59_1_1.gridx = 17;
		gbc_panel_59_1_1.gridy = 0;
		panel_23.add(panel_59_1_1, gbc_panel_59_1_1);
		panel_59_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_29_1_1 = new JLabel("");
		lblNewLabel_29_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_59_1_1.add(lblNewLabel_29_1_1);
		
		JPanel panel_60_1_1 = new JPanel();
		panel_60_1_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_60_1_1 = new GridBagConstraints();
		gbc_panel_60_1_1.gridwidth = 4;
		gbc_panel_60_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_60_1_1.gridx = 18;
		gbc_panel_60_1_1.gridy = 0;
		panel_23.add(panel_60_1_1, gbc_panel_60_1_1);
		panel_60_1_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_30_1_1 = new JLabel("");
		lblNewLabel_30_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_60_1_1.add(lblNewLabel_30_1_1);
		
		JPanel panel_24 = new JPanel();
		panel_24.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_24 = new GridBagConstraints();
		gbc_panel_24.fill = GridBagConstraints.BOTH;
		gbc_panel_24.gridx = 0;
		gbc_panel_24.gridy = 4;
		panel_2.add(panel_24, gbc_panel_24);
		GridBagLayout gbl_panel_24 = new GridBagLayout();
		gbl_panel_24.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_24.rowHeights = new int[]{ROW_BASE};
		gbl_panel_24.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_24.rowWeights = new double[]{1.0};
		panel_24.setLayout(gbl_panel_24);
		
		JLabel lblNewLabel_32 = new JLabel("合  计  (  大  写  ):");
		GridBagConstraints gbc_lblNewLabel_32 = new GridBagConstraints();
		gbc_lblNewLabel_32.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_32.gridwidth = 4;
		gbc_lblNewLabel_32.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_32.gridx = 0;
		gbc_lblNewLabel_32.gridy = 0;
		panel_24.add(lblNewLabel_32, gbc_lblNewLabel_32);
		
		lblPureWeightChinese = new JLabel("");
		lblPureWeightChinese.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblPureWeightChinese = new GridBagConstraints();
		gbc_lblPureWeightChinese.gridwidth = 18;
		gbc_lblPureWeightChinese.fill = GridBagConstraints.BOTH;
		gbc_lblPureWeightChinese.insets = new Insets(0, 0, 0, 5);
		gbc_lblPureWeightChinese.gridx = 4;
		gbc_lblPureWeightChinese.gridy = 0;
		panel_24.add(lblPureWeightChinese, gbc_lblPureWeightChinese);
		
		JPanel panel_25 = new JPanel();
		panel_25.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_25 = new GridBagConstraints();
		gbc_panel_25.fill = GridBagConstraints.BOTH;
		gbc_panel_25.gridx = 0;
		gbc_panel_25.gridy = 5;
		panel_2.add(panel_25, gbc_panel_25);
		GridBagLayout gbl_panel_25 = new GridBagLayout();
		gbl_panel_25.columnWidths = PanelTools.getArray(COLUMN_BASE, COLUMN_COUNT);
		gbl_panel_25.rowHeights = new int[]{ROW_BASE};
		gbl_panel_25.columnWeights = PanelTools.getArray(1.0, COLUMN_COUNT);
		gbl_panel_25.rowWeights = new double[]{1.0};
		panel_25.setLayout(gbl_panel_25);
		
		JLabel lblNewLabel_33 = new JLabel("备注:");
		GridBagConstraints gbc_lblNewLabel_33 = new GridBagConstraints();
		gbc_lblNewLabel_33.gridwidth = 2;
		gbc_lblNewLabel_33.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_33.gridx = 0;
		gbc_lblNewLabel_33.gridy = 0;
		panel_25.add(lblNewLabel_33, gbc_lblNewLabel_33);
		
		Component horizontalGlue_5 = Box.createHorizontalGlue();
		GridBagConstraints gbc_horizontalGlue_5 = new GridBagConstraints();
		gbc_horizontalGlue_5.gridwidth = 20;
		gbc_horizontalGlue_5.gridx = 2;
		gbc_horizontalGlue_5.gridy = 0;
		panel_25.add(horizontalGlue_5, gbc_horizontalGlue_5);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(Color.BLUE));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.gridwidth = 22;
		gbc_panel_3.gridheight = 2;
		gbc_panel_3.insets = new Insets(0, 0, 0, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 13;
		add(panel_3, gbc_panel_3);
		panel_3.setLayout(new GridLayout(2, 0, 0, 0));
		
		JPanel panel_62 = new JPanel();
		panel_3.add(panel_62);
		panel_62.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel_64 = new JPanel();
		panel_62.add(panel_64);
		panel_64.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_3 = new JLabel("负责人:");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		panel_64.add(lblNewLabel_3);
		
		Component horizontalGlue_6 = Box.createHorizontalGlue();
		panel_64.add(horizontalGlue_6);
		
		JPanel panel_65 = new JPanel();
		panel_62.add(panel_65);
		panel_65.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_34 = new JLabel("检验员:");
		lblNewLabel_34.setHorizontalAlignment(SwingConstants.LEFT);
		panel_65.add(lblNewLabel_34);
		
		Component horizontalGlue_7 = Box.createHorizontalGlue();
		panel_65.add(horizontalGlue_7);
		
		JPanel panel_66 = new JPanel();
		panel_62.add(panel_66);
		panel_66.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_35 = new JLabel("过称员:");
		lblNewLabel_35.setHorizontalAlignment(SwingConstants.LEFT);
		panel_66.add(lblNewLabel_35);
		
		Component horizontalGlue_8 = Box.createHorizontalGlue();
		panel_66.add(horizontalGlue_8);
		
		JPanel panel_63 = new JPanel();
		panel_3.add(panel_63);
		panel_63.setLayout(new GridLayout(1, 0, 0, 0));

	}
}
