package org.grain.view.print;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;

public class UserLinePanel extends JPanel {
	private int baseRowHeight;
	private int baseColumnLength;
	private int baseColumnCount;
	
	public UserLinePanel() {
		this(35, 35, 22);
	}
	/**
	 * Create the panel.
	 */
	public UserLinePanel(int rowHeight, int columnLength, int columnCount) {
		this.baseRowHeight = rowHeight;
		this.baseColumnLength = columnLength;
		this.baseColumnCount = columnCount;

		setPreferredSize(new Dimension(baseColumnCount * baseColumnLength, baseRowHeight));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = PanelTools.getArray(baseColumnLength, baseColumnCount);
		gridBagLayout.columnWeights = PanelTools.getArray(1.0, baseColumnCount);
		gridBagLayout.rowHeights = new int[] {baseRowHeight};
		gridBagLayout.rowWeights = new double[]{1.0};
		setLayout(gridBagLayout);
		
		JPanel panel_line_usr = new JPanel();
		panel_line_usr.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_line_usr = new GridBagConstraints();
		gbc_panel_line_usr.gridwidth = 8;
		gbc_panel_line_usr.fill = GridBagConstraints.BOTH;
		gbc_panel_line_usr.gridx = 0;
		gbc_panel_line_usr.gridy = 0;
		add(panel_line_usr, gbc_panel_line_usr);
		GridBagLayout gbl_panel_line_usr = new GridBagLayout();
		gbl_panel_line_usr.columnWidths = PanelTools.getArray(gbc_panel_line_usr.gridwidth * baseColumnLength / 4, 4);
		gbl_panel_line_usr.rowHeights = new int[] {baseRowHeight};
//		gbl_panel.columnWeights = new double[]{1, 1 ,1, 1};
		gbl_panel_line_usr.rowWeights = new double[]{1.0};
		panel_line_usr.setLayout(gbl_panel_line_usr);
		
		JLabel lblUsrLabel = new JLabel("姓  名:");
		GridBagConstraints gbc_lblUsrLabel = new GridBagConstraints();
		gbc_lblUsrLabel.fill = GridBagConstraints.BOTH;
		gbc_lblUsrLabel.insets = new Insets(0, 0, 0, 0);
		gbc_lblUsrLabel.gridwidth = 1;
		gbc_lblUsrLabel.gridx = 0;
		gbc_lblUsrLabel.gridy = 0;
		panel_line_usr.add(lblUsrLabel, gbc_lblUsrLabel);
		
		Component horizontalGlueUsr = Box.createHorizontalGlue();
		GridBagConstraints gbc_horizontalGlueUsr = new GridBagConstraints();
		gbc_horizontalGlueUsr.gridwidth = 3;
		gbc_horizontalGlueUsr.insets = new Insets(0, 0, 0, 0);
		gbc_horizontalGlueUsr.gridx = 1;
		gbc_horizontalGlueUsr.gridy = 0;
		panel_line_usr.add(horizontalGlueUsr, gbc_horizontalGlueUsr);
		
		JPanel panel_usr_location = new JPanel();
		panel_usr_location.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_usr_location = new GridBagConstraints();
		gbc_panel_usr_location.gridwidth = 10;
		gbc_panel_usr_location.fill = GridBagConstraints.VERTICAL;
		gbc_panel_usr_location.gridx = 8;
		gbc_panel_usr_location.gridy = 0;
		add(panel_usr_location, gbc_panel_usr_location);
		GridBagLayout gbl_panel_usr_location = new GridBagLayout();
		gbl_panel_usr_location.columnWidths = PanelTools.getArray(gbc_panel_usr_location.gridwidth * baseColumnLength / 5, 5);
		gbl_panel_usr_location.rowHeights = new int[]{baseRowHeight};
//		gbl_panel_1.columnWeights = new double[]{1, 1, 1, 1, 1};
		gbl_panel_usr_location.rowWeights = new double[]{1.0};
		panel_usr_location.setLayout(gbl_panel_usr_location);
		
		JPanel panel_usr_location_lbl = new JPanel();
		GridBagConstraints gbc_panel_usr_location_lbl = new GridBagConstraints();
		gbc_panel_usr_location_lbl.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_location_lbl.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_usr_location_lbl.insets = new Insets(0, 0, 0, 0);
		gbc_panel_usr_location_lbl.gridx = 0;
		gbc_panel_usr_location_lbl.gridy = 0;
		panel_usr_location.add(panel_usr_location_lbl, gbc_panel_usr_location_lbl);
		panel_usr_location_lbl.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel_usr_location = new JLabel("单位:");
		panel_usr_location_lbl.add(lblNewLabel_usr_location);
		
		JPanel panel_usr_location_input = new JPanel();
		GridBagConstraints gbc_panel_usr_location_input = new GridBagConstraints();
		gbc_panel_usr_location_input.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_location_input.gridwidth = 4;
		gbc_panel_usr_location_input.gridx = 1;
		gbc_panel_usr_location_input.gridy = 0;
		panel_usr_location.add(panel_usr_location_input, gbc_panel_usr_location_input);
		GridBagLayout gbl_panel_usr_location_input = new GridBagLayout();
		gbl_panel_usr_location_input.columnWidths = new int[] {20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20};
		gbl_panel_usr_location_input.columnWidths = new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		int panel_4_width = gbc_panel_usr_location.gridwidth * baseColumnLength / 5 * gbc_panel_usr_location_input.gridwidth;
		panel_usr_location_input.setPreferredSize(new Dimension(panel_4_width, baseRowHeight));
		gbl_panel_usr_location_input.rowHeights = new int[] {baseRowHeight};
		gbl_panel_usr_location_input.rowWeights = new double[]{1.0};
		panel_usr_location_input.setLayout(gbl_panel_usr_location_input);
		
		Component horizontalBox_usr_lcation1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalBox_usr_lcation1 = new GridBagConstraints();
		gbc_horizontalBox_usr_lcation1.insets = new Insets(0, 0, 0, 0);
		gbc_horizontalBox_usr_lcation1.gridwidth = 7;
		gbc_horizontalBox_usr_lcation1.anchor = GridBagConstraints.WEST;
		gbc_horizontalBox_usr_lcation1.fill = GridBagConstraints.VERTICAL;
		gbc_horizontalBox_usr_lcation1.gridx = 0;
		gbc_horizontalBox_usr_lcation1.gridy = 0;
		panel_usr_location_input.add(horizontalBox_usr_lcation1, gbc_horizontalBox_usr_lcation1);
		
		JLabel lblNewLabel_usr_location1 = new JLabel("乡");
		GridBagConstraints gbc_lblNewLabel_usr_location1 = new GridBagConstraints();
		gbc_lblNewLabel_usr_location1.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_usr_location1.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_usr_location1.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel_usr_location1.gridx = 7;
		gbc_lblNewLabel_usr_location1.gridy = 0;
		panel_usr_location_input.add(lblNewLabel_usr_location1, gbc_lblNewLabel_usr_location1);
		
		Component horizontalBox_usr_lcation12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalBox_usr_lcation12 = new GridBagConstraints();
		gbc_horizontalBox_usr_lcation12.fill = GridBagConstraints.VERTICAL;
		gbc_horizontalBox_usr_lcation12.gridwidth = 2;
		gbc_horizontalBox_usr_lcation12.anchor = GridBagConstraints.WEST;
		gbc_horizontalBox_usr_lcation12.insets = new Insets(0, 0, 0, 0);
		gbc_horizontalBox_usr_lcation12.gridx = 8;
		gbc_horizontalBox_usr_lcation12.gridy = 0;
		panel_usr_location_input.add(horizontalBox_usr_lcation12, gbc_horizontalBox_usr_lcation12);
		
		JLabel lblNewLabel_usr_location2 = new JLabel("村");
		GridBagConstraints gbc_lblNewLabel_usr_location2 = new GridBagConstraints();
		gbc_lblNewLabel_usr_location2.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_usr_location2.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_usr_location2.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel_usr_location2.gridx = 10;
		gbc_lblNewLabel_usr_location2.gridy = 0;
		panel_usr_location_input.add(lblNewLabel_usr_location2, gbc_lblNewLabel_usr_location2);
		
		Component horizontalBox_usr_lcation3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalBox_usr_lcation3 = new GridBagConstraints();
		gbc_horizontalBox_usr_lcation3.gridwidth = 2;
		gbc_horizontalBox_usr_lcation3.anchor = GridBagConstraints.WEST;
		gbc_horizontalBox_usr_lcation3.fill = GridBagConstraints.VERTICAL;
		gbc_horizontalBox_usr_lcation3.insets = new Insets(0, 0, 0, 0);
		gbc_horizontalBox_usr_lcation3.gridx = 11;
		gbc_horizontalBox_usr_lcation3.gridy = 0;
		panel_usr_location_input.add(horizontalBox_usr_lcation3, gbc_horizontalBox_usr_lcation3);
		
		JLabel lblNewLabel_usr_location3 = new JLabel("组");
		GridBagConstraints gbc_lblNewLabel_usr_location3 = new GridBagConstraints();
		gbc_lblNewLabel_usr_location3.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel_usr_location3.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_usr_location3.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_usr_location3.gridx = 13;
		gbc_lblNewLabel_usr_location3.gridy = 0;
		panel_usr_location_input.add(lblNewLabel_usr_location3, gbc_lblNewLabel_usr_location3);
		
		JPanel panel_usr_ordere = new JPanel();
		panel_usr_ordere.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_usr_ordere = new GridBagConstraints();
		gbc_panel_usr_ordere.gridwidth = 4;
		gbc_panel_usr_ordere.fill = GridBagConstraints.BOTH;
		gbc_panel_usr_ordere.gridx = 18;
		gbc_panel_usr_ordere.gridy = 0;
		add(panel_usr_ordere, gbc_panel_usr_ordere);
		
		GridBagLayout gbl_panel_usr_ordere = new GridBagLayout();
		gbl_panel_usr_ordere.columnWidths = PanelTools.getArray(baseColumnLength * 2, 2);
		gbl_panel_usr_ordere.rowHeights = new int[]{baseRowHeight};
//		gbl_panel_5.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_usr_ordere.rowWeights = new double[]{1.0};
		panel_usr_ordere.setLayout(gbl_panel_usr_ordere);
		
		JLabel lblNewLabel_usr_order_lbl = new JLabel("订单号:");
		lblNewLabel_usr_order_lbl.setPreferredSize(new Dimension(baseColumnLength, baseRowHeight));
		GridBagConstraints gbc_lblNewLabel_usr_order_lbl = new GridBagConstraints();
		gbc_lblNewLabel_usr_order_lbl.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_usr_order_lbl.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel_usr_order_lbl.gridx = 0;
		gbc_lblNewLabel_usr_order_lbl.gridy = 0;
		panel_usr_ordere.add(lblNewLabel_usr_order_lbl, gbc_lblNewLabel_usr_order_lbl);
		
		Box horizontalBox_usr_order = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox_usr_order = new GridBagConstraints();
		gbc_horizontalBox_usr_order.fill = GridBagConstraints.BOTH;
		gbc_horizontalBox_usr_order.gridx = 1;
		gbc_horizontalBox_usr_order.gridy = 0;
		panel_usr_ordere.add(horizontalBox_usr_order, gbc_horizontalBox_usr_order);

	}
}
