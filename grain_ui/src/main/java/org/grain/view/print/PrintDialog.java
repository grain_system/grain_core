package org.grain.view.print;

import java.awt.BorderLayout;
import java.awt.Dialog;

import javax.swing.JDialog;

import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;

public class PrintDialog extends JDialog {

	private PrintPanel contentPanel = new PrintPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PrintDialog dialog = new PrintDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PrintDialog() {
		this(null, false, null, null, "");
	}
	/**
	 * Create the dialog.
	 */
	public PrintDialog(Dialog parent, boolean modal, Order order, User user, String carInfo) {
		super(parent, "", modal);
		setTitle("打印订单详情界面" + ((order == null) ? "" : ("-" + order.getId())));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 37 * 22, 40 * 15);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		contentPanel.updateOrder(order, user, carInfo);
	}

}
