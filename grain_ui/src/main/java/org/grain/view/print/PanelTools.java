package org.grain.view.print;

import java.util.stream.IntStream;

public class PanelTools {
	
	public static int[] getArray(int base, int count) {
		return IntStream.range(0,count).map(idx -> base).toArray();
	}

	public static double[] getArray(double base, int count) {
		return IntStream.range(0,count).mapToDouble(idx -> base).toArray();
	}
	
	public static int[] getArrayDyn(int base, int count) {
		int[] arr = IntStream.range(0,count + 1).map(idx -> base).toArray();
		arr[count] = 1;
		return arr;
	}

	public static double[] getArrayDyn(double base, int count) {
		double[] arr = IntStream.range(0,count + 1).mapToDouble(idx -> base).toArray();
		arr[count] = 1.0; 
		return arr;
	}
}
