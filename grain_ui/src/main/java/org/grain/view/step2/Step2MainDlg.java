package org.grain.view.step2;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.Dialog;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.border.LineBorder;

import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.api.impl.StandardImpl;
import org.grain.common.constant.Sex;
import org.grain.common.constant.State;
import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.Order;
import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.http.GrainRequestService;
import org.grain.view.DoubleKeyAdapt;
import org.grain.view.IntKeyAdapt;
import org.grain.view.PopupComboxDlg;
import org.grain.view.helperimpl.OrderRowHelper;
import org.grain.view.helperimpl.UserRowHelper;

import lombok.extern.slf4j.Slf4j;

import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

@Slf4j
public class Step2MainDlg extends JDialog {
    private static final String[] CAR_TYPES = { "拖拉机", "三轮车", "摩托车", "小汽车", "卡车", "货车", "畜力车", "自行车", "其他" };
	private static StandardApi standardApi = new StandardImpl();
	private static State QUERY_STATE = State.BEGIN;
	private JTextField birthTextField;
	private JTextField idNumTextField;
	private JTextField idNameTextField;
	private JTextField idNationtextField;
	private JTextField carNumTextField;
	private JTextField weightTextField;
	private GrainRequestService grainRequestService;
	private JTextPane textShowInfoPane;
	private JRadioButton rdbtnSexMaleRadioButton;
	private JRadioButton rdbtnFemaleRadioButton;
	
	private Order lockedOrder;
	private OrderRowHelper lockedRowHelper;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Step2MainDlg dialog = new Step2MainDlg();
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public Step2MainDlg() {
		this(null, false);
	}

	public Step2MainDlg(Dialog parent, boolean modal) {
		super(parent, "", modal);
		setTitle("步骤2:选择处理订单,录入车总重量");
		setBounds(100, 100, 730, 525);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		grainRequestService = new GrainRequestService();
		getContentPane().setLayout(new GridLayout(1, 0, 1, 0));

		JPanel rootPanel = new JPanel();
		getContentPane().add(rootPanel);
		GridBagLayout gbl_rootPanel = new GridBagLayout();
		gbl_rootPanel.columnWidths = new int[] { 700, 0 };
		gbl_rootPanel.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
		gbl_rootPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_rootPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		rootPanel.setLayout(gbl_rootPanel);

		ButtonGroup sexBtnGroup = new ButtonGroup();

		JPanel orderPanel = new JPanel();
		orderPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 0, 0)), "\u8BA2\u5355\u4FE1\u606F",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		GridBagConstraints gbc_orderPanel = new GridBagConstraints();
		gbc_orderPanel.gridheight = 6;
		gbc_orderPanel.fill = GridBagConstraints.BOTH;
		gbc_orderPanel.insets = new Insets(0, 0, 5, 0);
		gbc_orderPanel.gridx = 0;
		gbc_orderPanel.gridy = 0;
		rootPanel.add(orderPanel, gbc_orderPanel);
		orderPanel.setLayout(new GridLayout(2, 0, 2, 2));

		JPanel panel_18 = new JPanel();
		orderPanel.add(panel_18);
		panel_18.setLayout(new GridLayout(4, 1, 0, 0));

		JPanel idNumPanel = new JPanel();

		JPanel idNumLabelPanel = new JPanel();
		idNumPanel.add(idNumLabelPanel);
		idNumLabelPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel idNumLabel = new JLabel("身份证号(*):");
		idNumLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idNumLabelPanel.add(idNumLabel);

		panel_18.add(idNumPanel);
		idNumPanel.setLayout(new GridLayout(0, 4, 0, 0));

		idNumTextField = new JTextField();
		idNumTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				log.debug("key typed " + e.getKeyChar());
				Character keyCh = e.getKeyChar();
				Pattern pat = Pattern.compile("[0-9]|[X]");
				if (!pat.matcher(String.valueOf(keyCh)).matches()) {
					if (keyCh != ' ') {// 回车字符
						e.setKeyChar('\0');
					}
				}
				if (idNumTextField.getText().length() >= 20) {
					e.setKeyChar('\0');
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				updateBirthDay();
			}
		});
		idNumPanel.add(idNumTextField);
		idNumTextField.setColumns(10);

		btnIdNumButton = new JButton("身份证查询");
		btnIdNumButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnIdNumButton.setEnabled(false);
				try {
					String number = idNumTextField.getText();
					if (number == null || number.length() < 3) {
						JOptionPane.showMessageDialog(Step2MainDlg.this, "请至少输入3位身份证号!", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
					List<User> users = grainRequestService.searchUsers(number);
					log.info("the user count:" + users.size());
					if (users.isEmpty()) {
						textShowInfoPane.setText("未找到匹配用户,请确认!");
						JOptionPane.showMessageDialog(Step2MainDlg.this, "未找到匹配用户,请确认", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
					PopupComboxDlg popupComboxDlg = new PopupComboxDlg(Step2MainDlg.this, true, (List<?>) users,
							new UserRowHelper());
					popupComboxDlg.setVisible(true);

					if (popupComboxDlg.isSelect() && popupComboxDlg.getSelectObj() != null) {
						User user = (User) popupComboxDlg.getSelectObj();
						log.info("select the user!" + user.toString());
						updateUserInfo(user);
					}

				} catch (Exception exp) {
					log.error("search user failed!" + exp.toString());
					textShowInfoPane.setText("查找身份证号出现异常" + exp.toString());

				} finally {
					btnIdNumButton.setEnabled(true);
				}
			}
		});
		idNumPanel.add(btnIdNumButton);

		btnSearchOrderByIdButton = new JButton("搜索身份证关联订单号");
		btnSearchOrderByIdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean recoveryBtnStatus = true;
				btnSearchOrderByIdButton.setEnabled(false);
				try {
					String number = idNumTextField.getText();
					if (number == null || number.length() != 18) {
						JOptionPane.showMessageDialog(Step2MainDlg.this, "请输入18位身份证号!", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
					List<User> users = grainRequestService.searchUsers(number);
					if (users.size() != 1) {
						JOptionPane.showMessageDialog(Step2MainDlg.this, "身份证号不唯一,请重新搜索!", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					long curDateLong = SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())).getTime();

					List<Order> orders = grainRequestService.searchOrders(number, QUERY_STATE, new Timestamp(curDateLong));
					recoveryBtnStatus = updateSelectOrder(orders, users.get(0));
				} catch (Exception exp) {
					log.error("search user failed!" + exp.toString());
					textShowInfoPane.setText("查找身份证号出现异常" + exp.toString());

				} finally {
					if (recoveryBtnStatus) {
						btnSearchOrderByIdButton.setEnabled(true);
					}
				}
			}
		});
		idNumPanel.add(btnSearchOrderByIdButton);

		JPanel orderSelectPanel = new JPanel();
		panel_18.add(orderSelectPanel);
		orderSelectPanel.setLayout(new GridLayout(0, 4, 0, 0));

		JLabel lblNewLabel_7 = new JLabel("订单号:");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.CENTER);
		orderSelectPanel.add(lblNewLabel_7);

		orderSelectTextField = new JTextField();
		orderSelectPanel.add(orderSelectTextField);
		orderSelectTextField.setColumns(10);

		btnSearchOrderButton = new JButton("根据订单号搜索");
		btnSearchOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean recoveryBtnStatus = true;
				btnSearchOrderButton.setEnabled(false);
				try {
					String orderIdStr = orderSelectTextField.getText();
					if (StringUtils.isEmpty(orderIdStr)) {
						JOptionPane.showMessageDialog(Step2MainDlg.this, "订单号不能为空!", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
					Pattern pat = Pattern.compile("^[0-9]*$");
					if (!pat.matcher(orderIdStr).matches()) {
						JOptionPane.showMessageDialog(Step2MainDlg.this, "订单号只能为数字!", "标题",
								JOptionPane.WARNING_MESSAGE);
						return;
					}

					long orderId = Long.parseLong(orderIdStr);

					List<Order> orders = grainRequestService.searchOrders(orderId, QUERY_STATE);
					User user = null;
					if (!orders.isEmpty()) {
						user = grainRequestService.searchUsers(orders.get(0).getOwner()).get(0);
					}
					recoveryBtnStatus = updateSelectOrder(orders, user);
				} catch (Exception exp) {
					log.error("search user failed!" + exp.toString());
					textShowInfoPane.setText("查找身份证号出现异常" + exp.toString());

				} finally {
					if (recoveryBtnStatus) {
						btnSearchOrderButton.setEnabled(true);
					}
				}
			}
		});
		orderSelectPanel.add(btnSearchOrderButton);
		
		Component horizontalGlue_4 = Box.createHorizontalGlue();
		orderSelectPanel.add(horizontalGlue_4);

		JPanel idNamePanel = new JPanel();
		panel_18.add(idNamePanel);
		idNamePanel.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel panel = new JPanel();
		idNamePanel.add(panel);

		JLabel lblIdNameLabel = new JLabel("姓名(*):");
		panel.add(lblIdNameLabel);

		JPanel panel_1 = new JPanel();
		idNamePanel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));

		idNameTextField = new JTextField();
		idNameTextField.setEditable(false);
		panel_1.add(idNameTextField);
		idNameTextField.setColumns(10);

		JPanel panel_7 = new JPanel();
		idNamePanel.add(panel_7);
		panel_7.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblNewLabel = new JLabel("生日");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_7.add(lblNewLabel);

		JPanel panel_8 = new JPanel();
		idNamePanel.add(panel_8);
		panel_8.setLayout(new GridLayout(0, 1, 0, 0));

		birthTextField = new JTextField();
		birthTextField.setEditable(false);
		birthTextField.setHorizontalAlignment(SwingConstants.LEFT);
		panel_8.add(birthTextField);
		birthTextField.setColumns(10);

		JPanel sexAndNationPanel = new JPanel();
		panel_18.add(sexAndNationPanel);
		sexAndNationPanel.setLayout(new GridLayout(1, 2, 2, 0));

		JPanel panel_2 = new JPanel();
		sexAndNationPanel.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblSexLabel = new JLabel("性别");
		lblSexLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblSexLabel);

		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);

		rdbtnSexMaleRadioButton = new JRadioButton("男");
		rdbtnSexMaleRadioButton.setEnabled(false);
		rdbtnSexMaleRadioButton.setSelected(true);
		rdbtnSexMaleRadioButton.setHorizontalAlignment(SwingConstants.CENTER);

		rdbtnFemaleRadioButton = new JRadioButton("女");
		rdbtnFemaleRadioButton.setEnabled(false);
		rdbtnFemaleRadioButton.setHorizontalAlignment(SwingConstants.CENTER);
		sexBtnGroup.add(rdbtnSexMaleRadioButton);
		sexBtnGroup.add(rdbtnFemaleRadioButton);

		panel_6.setLayout(new GridLayout(0, 2, 0, 0));
		panel_6.add(rdbtnSexMaleRadioButton);
		panel_6.add(rdbtnFemaleRadioButton);

		JPanel panel_3 = new JPanel();
		sexAndNationPanel.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5);
		panel_5.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblNewLabel_1 = new JLabel("民族:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_5.add(lblNewLabel_1);

		idNationtextField = new JTextField();
		idNationtextField.setEditable(false);
		panel_5.add(idNationtextField);
		idNationtextField.setColumns(10);

		JPanel carPanel = new JPanel();
		carPanel.setBorder(new TitledBorder(new LineBorder(new Color(0, 255, 0)), "\u8F66\u8F86\u4FE1\u606F",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		orderPanel.add(carPanel);
		carPanel.setLayout(new GridLayout(2, 0, 0, 0));

		JPanel panel_9 = new JPanel();
		carPanel.add(panel_9);
		panel_9.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel panel_15 = new JPanel();
		panel_9.add(panel_15);
		panel_15.setLayout(new GridLayout(0, 3, 2, 0));

		JPanel panel_11 = new JPanel();
		panel_15.add(panel_11);
		panel_11.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblNewLabel_2 = new JLabel("车牌号:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_11.add(lblNewLabel_2);

		carNumTextField = new JTextField();
		carNumTextField.setEditable(false);
		carNumTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				carNumTextField.setText(carNumTextField.getText().toUpperCase());
			}
		});
		carNumTextField.setHorizontalAlignment(SwingConstants.LEFT);
		panel_11.add(carNumTextField);
		carNumTextField.setColumns(10);

		JPanel panel_12 = new JPanel();
		panel_15.add(panel_12);
		panel_12.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblNewLabel_3 = new JLabel("车辆类型:");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		panel_12.add(lblNewLabel_3);

		carTypecomboBox = new JComboBox();
		
		for (String carType : CAR_TYPES) {
			carTypecomboBox.addItem(carType);
		}
		carTypecomboBox.setEnabled(false);
		panel_12.add(carTypecomboBox);

		JPanel panel_13 = new JPanel();
		panel_15.add(panel_13);
		panel_13.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblNewLabel_4 = new JLabel("车辆颜色:");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		panel_13.add(lblNewLabel_4);

		carColorTextField = new JTextField();
		carColorTextField.setEditable(false);
		panel_13.add(carColorTextField);
		carColorTextField.setColumns(10);

		Component verticalGlue = Box.createVerticalGlue();
		panel_9.add(verticalGlue);

		JPanel panel_10 = new JPanel();
		carPanel.add(panel_10);
		panel_10.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new TitledBorder(new LineBorder(new Color(192, 192, 192)),
				"\u8F66\u8F86\u63CF\u8FF0\u4FE1\u606F(\u6709\u8F66\u724C\u53F7\u6B64\u5904\u53EF\u4E0D\u586B)",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_10.add(panel_14);
		panel_14.setLayout(new GridLayout(0, 1, 0, 0));

		editorPane = new JEditorPane();
		editorPane.setEditable(false);
		panel_14.add(editorPane);

		JPanel weightPanel = new JPanel();
		weightPanel.setBorder(new TitledBorder(new EmptyBorder(0, 0, 0, 0), "\u5C0F\u9EA6\u54C1\u8D28\u5F55\u5165",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_weightPanel = new GridBagConstraints();
		gbc_weightPanel.gridheight = 6;
		gbc_weightPanel.fill = GridBagConstraints.BOTH;
		gbc_weightPanel.insets = new Insets(0, 0, 5, 0);
		gbc_weightPanel.gridx = 0;
		gbc_weightPanel.gridy = 6;
		rootPanel.add(weightPanel, gbc_weightPanel);
		weightPanel.setLayout(new GridLayout(3, 1, 0, 3));

		JPanel panel_16 = new JPanel();
		weightPanel.add(panel_16);
		panel_16.setLayout(new GridLayout(0, 4, 5, 0));


		
		JPanel panel_20 = new JPanel();
		panel_20.setBorder(new LineBorder(Color.PINK));
		panel_16.add(panel_20);
		panel_20.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_5 = new JLabel("总重量(千克):");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		panel_20.add(lblNewLabel_5);

		weightTextField = new JTextField();
		weightTextField.setEditable(false);
		panel_20.add(weightTextField);
		weightTextField.setColumns(10);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel_20.add(horizontalGlue);
		
		JPanel panel_21 = new JPanel();
		panel_21.setBorder(new LineBorder(Color.MAGENTA));
		panel_16.add(panel_21);
		panel_21.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_8 = new JLabel("容重(g/L):");
		panel_21.add(lblNewLabel_8);
		
		textFieldBulkDensity = new JTextField();
		textFieldBulkDensity.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				updateStandard(textFieldBulkDensity.getText());
			}
		});
		textFieldBulkDensity.addKeyListener(new IntKeyAdapt(textFieldBulkDensity));
		textFieldBulkDensity.setEnabled(false);
		panel_21.add(textFieldBulkDensity);
		textFieldBulkDensity.setColumns(10);
		
		JLabel lblNewLabel_9 = new JLabel("质量等级:");
		panel_21.add(lblNewLabel_9);
		
		textFieldBulkDensityStd = new JTextField();
		textFieldBulkDensityStd.setText("未匹配");
		textFieldBulkDensityStd.setEditable(false);
		panel_21.add(textFieldBulkDensityStd);
		textFieldBulkDensityStd.setColumns(10);
		
		JPanel panel_22 = new JPanel();
		panel_22.setBorder(new LineBorder(Color.ORANGE));
		panel_16.add(panel_22);
		panel_22.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_8_1 = new JLabel("不完粒(%):");
		panel_22.add(lblNewLabel_8_1);
		
		textFieldIncompleteGrain = new JTextField();
		textFieldIncompleteGrain.addKeyListener(new DoubleKeyAdapt(textFieldIncompleteGrain));
		textFieldIncompleteGrain.setEnabled(false);
		textFieldIncompleteGrain.setColumns(10);
		panel_22.add(textFieldIncompleteGrain);
		
		JLabel lblNewLabel_9_1 = new JLabel("标准:");
		panel_22.add(lblNewLabel_9_1);
		
		textFieldIncompleteGrainStd = new JTextField();
		textFieldIncompleteGrainStd.setEditable(false);
		panel_22.add(textFieldIncompleteGrainStd);
		textFieldIncompleteGrainStd.setColumns(10);
		
		JPanel panel_23 = new JPanel();
		panel_23.setBorder(new LineBorder(Color.GREEN));
		panel_16.add(panel_23);
		panel_23.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_8_1_1 = new JLabel("杂质(%):");
		panel_23.add(lblNewLabel_8_1_1);
		
		textFieldImpurity = new JTextField();
		textFieldImpurity.addKeyListener(new DoubleKeyAdapt(textFieldImpurity));
		textFieldImpurity.setEnabled(false);
		textFieldImpurity.setColumns(10);
		panel_23.add(textFieldImpurity);
		
		JLabel lblNewLabel_9_1_1 = new JLabel("标准:");
		panel_23.add(lblNewLabel_9_1_1);
		
		textFieldImpurityStd = new JTextField();
		textFieldImpurityStd.setEditable(false);
		panel_23.add(textFieldImpurityStd);
		textFieldImpurityStd.setColumns(10);

		
		JPanel panel_19 = new JPanel();
		weightPanel.add(panel_19);
		panel_19.setLayout(new GridLayout(0, 4, 3, 0));
		
		JPanel panel_24 = new JPanel();
		panel_24.setBorder(new LineBorder(Color.BLUE));
		panel_19.add(panel_24);
		panel_24.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_11 = new JLabel("水份(%):");
		panel_24.add(lblNewLabel_11);
		
		textFieldMoisture = new JTextField();
		textFieldMoisture.addKeyListener(new DoubleKeyAdapt(textFieldMoisture));
		textFieldMoisture.setEnabled(false);
		textFieldMoisture.setColumns(10);
		panel_24.add(textFieldMoisture);
		
		JLabel lblNewLabel_10 = new JLabel("标准:");
		panel_24.add(lblNewLabel_10);
		
		textFieldMoistureStd = new JTextField();
		textFieldMoistureStd.setEditable(false);
		panel_24.add(textFieldMoistureStd);
		textFieldMoistureStd.setColumns(10);
		
		JPanel panel_25 = new JPanel();
		panel_25.setBorder(new LineBorder(Color.RED));
		panel_19.add(panel_25);
		panel_25.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_12 = new JLabel("霉变率(%):");
		panel_25.add(lblNewLabel_12);
		
		textFieldMildewRate = new JTextField();
		textFieldMildewRate.addKeyListener(new DoubleKeyAdapt(textFieldMildewRate));
		textFieldMildewRate.setEnabled(false);
		textFieldMildewRate.setText("0");
		textFieldMildewRate.setColumns(10);
		panel_25.add(textFieldMildewRate);
		
		JLabel lblNewLabel_13 = new JLabel("标准:");
		panel_25.add(lblNewLabel_13);
		
		textFieldMildewRateStd = new JTextField();
		textFieldMildewRateStd.setText("0");
		textFieldMildewRateStd.setEditable(false);
		panel_25.add(textFieldMildewRateStd);
		textFieldMildewRateStd.setColumns(10);

		textShowInfoPane = new JTextPane();
		textShowInfoPane.setEditable(false);
		textShowInfoPane.setForeground(Color.BLACK);
		textShowInfoPane.setText("提示信息。");
		textShowInfoPane.setFont(new Font("宋体", Font.PLAIN, 12));
		weightPanel.add(textShowInfoPane);

		JPanel textShowPanel = new JPanel();
		GridBagConstraints gbc_textShowPanel = new GridBagConstraints();
		gbc_textShowPanel.fill = GridBagConstraints.BOTH;
		gbc_textShowPanel.gridx = 0;
		gbc_textShowPanel.gridy = 12;
		rootPanel.add(textShowPanel, gbc_textShowPanel);
		textShowPanel.setLayout(new GridLayout(3, 1, 0, 0));

		JPanel panel_17 = new JPanel();
		textShowPanel.add(panel_17);
		panel_17.setLayout(new GridLayout(0, 5, 0, 0));

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_1);

		JButton btnResetButton = new JButton("重置");
		btnResetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearAllIputComponent();
				setEnableOrderControl(true);
				enableGrainControl(false);
				clearGrainControl();
				lockedOrder = null;
				lockedRowHelper = null;
				btnUpdateOrderButton.setEnabled(true);
			}
		});
		panel_17.add(btnResetButton);

		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_2);

		btnUpdateOrderButton = new JButton("更新订单");
		btnUpdateOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lockedOrder == null || lockedRowHelper == null) {
					String loginfo = "订单未锁定!";
					showInfoAndMessage(loginfo, loginfo);
					return;	
				}
				long orderId = lockedOrder.getId();
				Order curOrder;
				try {
					curOrder = loadOrder(orderId);
				} catch (Exception exp) {
					curOrder = null;
				}
				
				OperatorReason operatioReason = checkOrder(curOrder);
				if (!operatioReason.isSuccess()) {
					String loginfo = operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, loginfo);
					return;	
				}
				
				operatioReason = grainRequestService.addOrderStep1(curOrder);
				if (!operatioReason.isSuccess()) {
					String loginfo = "update order2 info failed" + operatioReason.getReason().toString();
					String message = "订单更新失败,原因：" + operatioReason.getReason().toString();
					showInfoAndMessage(loginfo, message);
					return;
				}
				
                weightTextField.setEditable(false);
				String loginfo = "order info insert success, id=" + orderId;
				log.info(loginfo);
				String message = "订单更新成功!订单ID号为:" + orderId;
				textShowInfoPane.setText(message);
				btnUpdateOrderButton.setEnabled(false);
			}
		});
		panel_17.add(btnUpdateOrderButton);

		Component horizontalGlue_3 = Box.createHorizontalGlue();
		panel_17.add(horizontalGlue_3);

		Component verticalGlue_2 = Box.createVerticalGlue();
		textShowPanel.add(verticalGlue_2);

		Component verticalGlue_3 = Box.createVerticalGlue();
		textShowPanel.add(verticalGlue_3);
	}
	
	private void updateStandard(String bulkDesityStr) {
		if (!StringUtils.isEmpty(bulkDesityStr)) {
			try {
				int bulkDesity = Integer.parseInt(bulkDesityStr);
				int level = standardApi.getLevel(bulkDesity);
				if (standardApi.validLevel(level)) {
					textFieldBulkDensityStd.setText("等级" + (level + 1));
					StandardLevelApi standardLevelApi = standardApi.getStandard(level);
					textFieldIncompleteGrainStd.setText("" + standardLevelApi.getIncompleteGrain());
					textFieldImpurityStd.setText("" + standardLevelApi.getImpurity());
					textFieldMoistureStd.setText("" + standardLevelApi.getMoisture());
					textFieldMildewRateStd.setText("0");
					return;
				}
			} catch (Exception exp) {
				log.warn("update standard with exp:" + exp.toString());
			}
		}
		textFieldBulkDensityStd.setText("未匹配");
		textFieldIncompleteGrainStd.setText("");
		textFieldImpurityStd.setText("");
		textFieldMoistureStd.setText("");
		textFieldMildewRateStd.setText("0");
	}

	private boolean updateSelectOrder(List<Order> orders, User user) {
		log.info("the user count:" + orders.size());
		if (orders.isEmpty()) {
			textShowInfoPane.setText("无有效订单,请确认!");
			JOptionPane.showMessageDialog(Step2MainDlg.this, "无有效订单,请确认", "标题",
					JOptionPane.WARNING_MESSAGE);
			return true;
		}
		OrderRowHelper rowHelper = new OrderRowHelper(grainRequestService, orders.size(), user); 
		PopupComboxDlg popupComboxDlg = new PopupComboxDlg(Step2MainDlg.this, true, (List<?>) orders,
				rowHelper);
		popupComboxDlg.setVisible(true);

		if (popupComboxDlg.isSelect() && popupComboxDlg.getSelectObj() != null) {
			Order order = (Order) popupComboxDlg.getSelectObj();
			log.info("select the order!" + order.toString());
			lockOrder(rowHelper, order);
			return false;
		}
		return true;
	}
	
	private void enableGrainControl(boolean enable) {
		textFieldBulkDensity.setEnabled(enable);
		textFieldIncompleteGrain.setEnabled(enable);
		textFieldImpurity.setEnabled(enable);
		textFieldMoisture.setEnabled(enable);
		textFieldMildewRate.setEnabled(enable);
	}
	
	private void clearGrainControl() {
		textFieldBulkDensity.setText("");
		textFieldIncompleteGrain.setText("");
		textFieldImpurity.setText("");
		textFieldMoisture.setText("");
		textFieldMildewRate.setText("0");
		
		textFieldBulkDensityStd.setText("未匹配");
		textFieldIncompleteGrainStd.setText("");
		textFieldImpurityStd.setText("");
		textFieldMoistureStd.setText("");
		textFieldMildewRateStd.setText("0");
	}
	
	private void lockOrder(OrderRowHelper rowHelper, Order order) {
		this.lockedOrder = order;
		this.lockedRowHelper = rowHelper;
		orderSelectTextField.setText("" + order.getId());
		weightTextField.setEditable(true);
		updateQuality(order);
		updateUserInfo(rowHelper.getUser());
		updateCarControl(rowHelper.getCar(order));
		setEnableOrderControl(false);
		textShowInfoPane.setText("已锁定订单号:" + order.getId());

	}
	
	private void updateQuality(Order order) {
		textFieldBulkDensity.setText("" + order.getBulkDensity());
		textFieldIncompleteGrain.setText("" + order.getIncompleteGrain());
		textFieldImpurity.setText("" + order.getImpurity());
		textFieldMoisture.setText("" + order.getMoisture());
		textFieldMildewRate.setText("" + order.getMildewRate());
		updateStandard(""+ order.getBulkDensity());
	}
	
	private void updateCarControl(Car car) {
		carNumTextField.setText(car.getCardNum());
		if (!StringUtils.isEmpty(car.getCarType())) {
			int index = 0;
			for (; index < CAR_TYPES.length; index ++) {
				if (CAR_TYPES[index].equals(car.getCarType())) {
					carTypecomboBox.setSelectedIndex(index);
					break;
				}
			}
		}
		carColorTextField.setText(car.getCarColor());
		editorPane.setText(car.getCarDesc());
	}
	
	private void setEnableOrderControl(boolean enable) {
		orderSelectTextField.setEnabled(enable);
		setCarControl(enable);
		setUserControl(enable);
		btnIdNumButton.setEnabled(enable);
		btnSearchOrderByIdButton.setEnabled(enable);
		btnSearchOrderButton.setEnabled(enable);
	}
	
	private void setCarControl(boolean enable) {
		carNumTextField.setEnabled(enable);
		carColorTextField.setEnabled(enable);
		carTypecomboBox.setEnabled(enable);
		editorPane.setEnabled(enable);
	}
	
	private void setUserControl(boolean enable) {
		idNumTextField.setEnabled(enable);
		idNameTextField.setEnabled(enable);
		idNationtextField.setEnabled(enable);
	}
	
	
	private void showInfoAndMessage(String loginfo, String message) {
		log.warn(loginfo);
		textShowInfoPane.setText(message);
		JOptionPane.showMessageDialog(Step2MainDlg.this, message, "标题", JOptionPane.WARNING_MESSAGE);
	}
	
	private Order loadOrder(long orderId) {
		Order order = new Order();
		order.setId(orderId);
		order.setTotalWeight(Double.parseDouble(weightTextField.getText()));
		return order;
	}
	
	private static OperatorReason checkOrder(Order order) {
		return Order.checkStep1(order);
	}

	protected void clearAllIputComponent() {
		// clear user
		idNumTextField.setText("");
		idNameTextField.setText("");
		birthTextField.setText("");
		idNationtextField.setText("");
		rdbtnSexMaleRadioButton.setSelected(true);
		// clear car
		carNumTextField.setText("");
		carTypecomboBox.setSelectedIndex(-1);
		carColorTextField.setText("");
		editorPane.setText("");

		// clear weight
		weightTextField.setText("");
		textShowInfoPane.setText("提示信息。");
		orderSelectTextField.setText("");
	}

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private JComboBox carTypecomboBox;
	private JTextField carColorTextField;
	private JEditorPane editorPane;
	private JButton btnUpdateOrderButton;
	private JTextField orderSelectTextField;
	private JButton btnIdNumButton;
	private JButton btnSearchOrderByIdButton;
	private JButton btnSearchOrderButton;
	private JTextField textFieldBulkDensity;
	private JTextField textFieldIncompleteGrain;
	private JTextField textFieldImpurity;
	private JTextField textFieldMoisture;
	private JTextField textFieldMildewRate;
	private JTextField textFieldBulkDensityStd;
	private JTextField textFieldIncompleteGrainStd;
	private JTextField textFieldImpurityStd;
	private JTextField textFieldMoistureStd;
	private JTextField textFieldMildewRateStd;

	protected void updateBirthDay() {
		String curIdNum = idNumTextField.getText();
		if (curIdNum != null && curIdNum.length() >= 14) {
			Pattern pat = Pattern.compile("[0-9]{8}");
			String birthDay = curIdNum.substring(6, 14);
			if (pat.matcher(birthDay).matches()) {
				Date birthDate;
				try {
					birthDate = SIMPLE_DATE_FORMAT.parse(
							birthDay.substring(0, 4) + "-" + birthDay.substring(4, 6) + "-" + birthDay.substring(6, 8));
					birthTextField.setText(SIMPLE_DATE_FORMAT.format(birthDate));
					return;
				} catch (ParseException e) {
					log.error("parse birth day failed");
					e.printStackTrace();
				}
			}
		}
		birthTextField.setText("");
	}

	protected void updateUserInfo(User user) {
		if (user != null) {
			idNumTextField.setText(user.getId());
			idNameTextField.setText(user.getName());
			birthTextField.setText(user.getBirthday() == null ? "" : SIMPLE_DATE_FORMAT.format(user.getBirthday()));
			idNationtextField.setText(user.getNation() == null ? "" : user.getNation());
			Sex sex = user.getSex();
			if (sex != null && sex == Sex.FEMALE) {
				rdbtnFemaleRadioButton.setSelected(true);
			} else {
				rdbtnSexMaleRadioButton.setSelected(true);
			}
		}
	}

	protected JTextPane getTextShowInfoPane() {
		return textShowInfoPane;
	}

	protected JRadioButton getRdbtnSexMaleRadioButton() {
		return rdbtnSexMaleRadioButton;
	}

	protected JRadioButton getRdbtnFemaleRadioButton() {
		return rdbtnFemaleRadioButton;
	}
}
