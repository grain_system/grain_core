/**
 * 
 */
package org.grain.view;

import java.util.List;

/**
 * @author Administrator
 *
 */
public interface RowHelper {
	Object getColumn(Object obj, int column);
	List<String> getTitles();
	Integer getTitlesWidth(int column);
}
