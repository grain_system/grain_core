package org.grain.view;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Pattern;

import javax.swing.text.JTextComponent;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DoubleKeyAdapt extends IntKeyAdapt {
	private static Pattern validPattern = Pattern.compile("[0-9]|[.]");
	private static Pattern validFullPattern = Pattern.compile("([0-9]|[.]){0,}");
	public DoubleKeyAdapt(JTextComponent textComp) {
		super(textComp);
	}

	private boolean haveDot = false;

	@Override
	public void keyTyped(KeyEvent e) {
		super.keyTyped(e);
		Character keyCh = e.getKeyChar();
		if (keyCh == '.' && haveDot) {
			e.setKeyChar('\0');
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Pattern pat = getFullNumberPattern();
		String inputText = textComp.getText();
		if (!pat.matcher(inputText).matches()) {
			textComp.setText("");
			log.info("input number invalid");
			return;
		}
		if (StringUtils.countMatches(inputText, '.') > 1) {
			textComp.setText("");
			log.info("input number have more than one dot.");
			return;
		}
		if (textComp.getText().contains(".")) {
			haveDot = true;
		} else {
			haveDot = false;
		}
	}
	
	protected Pattern getNumberPattern() {
		return validPattern;
	}
	
	protected Pattern getFullNumberPattern() {
		return validFullPattern;
	}
}
