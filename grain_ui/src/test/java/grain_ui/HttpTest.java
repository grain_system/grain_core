package grain_ui;

import org.grain.http.Request;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpTest {
	String url = "http://localhost:8888/user?userId=6543";
	
	@Test
	public void testGet() throws Exception {
		Request request = new Request();
		String entity = request.doGet(url, null);
		log.info("the result=" + entity);
	}

}
