
drop table if exists grain_car;
drop table if exists grain_order;
drop table if exists grain_user;
-- grain_system.grain_car definition

CREATE TABLE grain_car (
  `car_id` bigint(20) NOT NULL,
  `card_num` varchar(10) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `car_type` varchar(10) DEFAULT NULL,
  `car_color` varchar(10) DEFAULT NULL,
  `car_desc` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`car_id`)
);



-- grain_system.grain_order definition

CREATE TABLE grain_order (
  `id` bigint(20) NOT NULL,
  `owner` varchar(32) NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `car_id` bigint(20) NOT NULL,
  `total_weight` double DEFAULT NULL,
  `do_weight_time` timestamp NULL DEFAULT NULL,
  `no_car_weight` double DEFAULT NULL,
  `addition_weight` double DEFAULT NULL,
  `bulk_density` int(11) DEFAULT NULL,
  `incomplete_grain` double DEFAULT NULL,
  `incomplete_grain_weight_per` double DEFAULT NULL,
  `impurity` double DEFAULT NULL,
  `impurity_weight_per` double DEFAULT NULL,
  `moisture` double DEFAULT NULL,
  `moisture_weight_per` double DEFAULT NULL,
  `mildew_rate` double DEFAULT NULL,
  `incr_total_weight` double DEFAULT NULL,
  `desc_total_weight` double DEFAULT NULL,
  `pure_total_weight` double DEFAULT NULL,
  `do_quality_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grain_order_owner_IDX` (`owner`,`state`,`create_time`) USING BTREE
);

-- grain_system.grain_user definition

CREATE TABLE grain_user (
  `id` varchar(32) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `name1` varchar(100) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `nation` varchar(8) DEFAULT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `signer` varchar(100) DEFAULT NULL,
  `valid_start` timestamp NULL DEFAULT NULL,
  `valid_end` char(1) DEFAULT NULL
);
