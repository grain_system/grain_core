CREATE TABLE grain_user (
    id character varying(32) NOT NULL,
    name character varying(100),
    name1 character varying(100),
    sex integer,
    nation character varying(8),
    birthday timestamp(0) without time zone,
    address character varying(200),
    signer character varying(100),
    valid_start timestamp(0) without time zone,
    valid_end character(1)
)
WITH (orientation=row, compression=no);
COMMENT ON COLUMN grain_user.id IS '身份证号';
COMMENT ON COLUMN grain_user.name IS '汉字名';
COMMENT ON COLUMN grain_user.name1 IS '其他名,如维语名';
COMMENT ON COLUMN grain_user.sex IS '性别0->女 1->男';
COMMENT ON COLUMN grain_user.nation IS '民族';
COMMENT ON COLUMN grain_user.birthday IS '出生日期';
COMMENT ON COLUMN grain_user.address IS '居住地址';
COMMENT ON COLUMN grain_user.signer IS '签证机关';
COMMENT ON COLUMN grain_user.valid_start IS '身份证有效期起始';
COMMENT ON COLUMN grain_user.valid_end IS '身份证有效期结束';
CREATE INDEX idx_user_name ON grain_user USING btree (name) WITH (fillfactor=100) TABLESPACE pg_default;
ALTER TABLE grain_user ADD CONSTRAINT grain_user_pkey PRIMARY KEY (id);

----
CREATE TABLE grain_order (
    id bigint NOT NULL,
    owner character varying(32) NOT NULL,
    create_time timestamp with time zone,
    state integer,
    car_id bigint NOT NULL,
    total_weight double precision,
    do_weight_time timestamp with time zone,
    no_car_weight double precision,
    addition_weight double precision,
    bulk_density integer,
    incomplete_grain double precision,
    incomplete_grain_weight_per double precision,
    impurity double precision,
    impurity_weight_per double precision,
    moisture double precision,
    moisture_weight_per double precision,
    mildew_rate double precision,
    incr_total_weight double precision,
    desc_total_weight double precision,
    pure_total_weight double precision,
    do_quality_time timestamp with time zone
)
WITH (orientation=row, compression=no);
COMMENT ON TABLE grain_order IS '订单表';
COMMENT ON COLUMN grain_order.id IS '订单id';
COMMENT ON COLUMN grain_order.owner IS '用户的身份证号';
COMMENT ON COLUMN grain_order.create_time IS '订单生成时间';
COMMENT ON COLUMN grain_order.state IS '当时流程单状态';
COMMENT ON COLUMN grain_order.car_id IS '关联的车唯一ID';
COMMENT ON COLUMN grain_order.total_weight IS '含有车的总重量';
COMMENT ON COLUMN grain_order.do_weight_time IS '去车称重时间';
COMMENT ON COLUMN grain_order.no_car_weight IS '去除车后的重量';
COMMENT ON COLUMN grain_order.addition_weight IS '袋皮重,没有则为0';
COMMENT ON COLUMN grain_order.bulk_density IS '容重，用于确认小麦等级,单位g/L';
COMMENT ON COLUMN grain_order.incomplete_grain IS '不完粒,单位%';
COMMENT ON COLUMN grain_order.incomplete_grain_weight_per IS '不完粒增减量,单位%';
COMMENT ON COLUMN grain_order.impurity IS '杂质,单位%';
COMMENT ON COLUMN grain_order.impurity_weight_per IS '杂质粒增减量,单位%';
COMMENT ON COLUMN grain_order.moisture IS '水份,单位%';
COMMENT ON COLUMN grain_order.moisture_weight_per IS '水粒增减量,单位%';
COMMENT ON COLUMN grain_order.mildew_rate IS '霉变率,%用于扣除减量';
COMMENT ON COLUMN grain_order.incr_total_weight IS '总增长重量,单位kg';
COMMENT ON COLUMN grain_order.desc_total_weight IS '总减少重量,单位kg';
COMMENT ON COLUMN grain_order.pure_total_weight IS '最终净含量,单位kg';
COMMENT ON COLUMN grain_order.do_quality_time IS '测量小麦质量后的录入时间';
ALTER TABLE grain_order ADD CONSTRAINT grain_order_pkey PRIMARY KEY (id);
CREATE INDEX index_order_car_id ON grain_order USING btree (car_id) WITH (fillfactor=100) TABLESPACE pg_default;
CREATE INDEX index_order_owner ON grain_order USING btree (owner) WITH (fillfactor=100) TABLESPACE pg_default;
ALTER TABLE grain_order ADD CONSTRAINT order_id_key UNIQUE (id);

----
CREATE TABLE grain_car (
	car_id bigint primary key,
	card_num character varying(10),
	user_id character varying(32),
	car_type character varying(10),
	car_color character varying(10),
	car_desc character varying(400)
)
WITH (orientation=row, compression=no);
COMMENT ON TABLE grain_car IS '这是用于描述货车的信息';
COMMENT ON COLUMN grain_car.car_id IS '这是车的唯一id';
COMMENT ON COLUMN grain_car.card_num IS '车牌号';
COMMENT ON COLUMN grain_car.user_id IS '拥有这量车的人的id';
COMMENT ON COLUMN grain_car.car_type IS '车辆类别,货车、汽车、三轮车、牛车，其他';
COMMENT ON COLUMN grain_car.car_color IS '车颜色，没有可以填未知，前端限制';
COMMENT ON COLUMN grain_car.car_desc IS '车辆描述,属于自填写信息';
CREATE INDEX index_car_userid ON grain_car USING btree (user_id) WITH (fillfactor=100) TABLESPACE pg_default;
CREATE INDEX index_car_cardnum ON grain_car USING btree (card_num) WITH (fillfactor=100) TABLESPACE pg_default;
ALTER TABLE grain_car ADD CONSTRAINT car_car_id_key UNIQUE (car_id);