/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Title: the GrainMvcCofnig class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@Configuration
public class GrainMvcConfig implements WebMvcConfigurer {
    @Override
    public void addFormatters(final FormatterRegistry registry) {
        registry.addConverterFactory(new EnumConvertFactroy());
    }
}
