/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.config;

import org.grain.common.constant.Sex;
import org.springframework.core.convert.converter.Converter;

/**
 * Title: the EnumConvert class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public class EnumConvert implements Converter<String, Sex> {
    
    @Override
    public Sex convert(final String source) {
        return Sex.getSexFromCode(source);
    }
}
