/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.config;

import org.grain.common.constant.Sex;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

/**
 * Title: the EnumConveryFactory class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public class EnumConvertFactroy implements ConverterFactory<String, Sex> {
    private Converter convert = new EnumConvert();
    @Override
    public <T extends Sex> Converter<String, T> getConverter(final Class<T> targetType) {
        return convert;
    }
}
