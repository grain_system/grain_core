/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.controller;

import com.sun.org.apache.xpath.internal.operations.Or;
import org.grain.common.domain.po.Order;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Title: the TestController class.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@RestController
@RequestMapping("order")
public class OrderController {
    @Resource
    private OrderService orderService;
    
    @GetMapping("/step1")
    public OperatorReason Step1(Order order) {
        OperatorReason operatorReason = orderService.checkStep1(order);
        if (!operatorReason.isSuccess()) return operatorReason;
        return orderService.updateStep1(order);
    }
    
    @GetMapping("/step2")
    public OperatorReason Step2(Order order) {
        OperatorReason operatorReason = orderService.checkStep2(order);
        if (!operatorReason.isSuccess()) return operatorReason;
        return orderService.updateStep2(order);
    }
    
    @GetMapping("/step3")
    public OperatorReason Step3(Order order) {
        OperatorReason operatorReason = orderService.checkStep3(order);
        if (!operatorReason.isSuccess())
            return operatorReason;
        return orderService.updateStep3(order);
    }

    @GetMapping("/step4")
    public OperatorReason Step4(Order order) {
        OperatorReason operatorReason = Order.checkStep2(order, orderService.standards);
        if (!operatorReason.isSuccess()) return operatorReason;
        operatorReason = Order.checkStep1(order);
        if (!operatorReason.isSuccess()) return operatorReason;
        operatorReason = order.checkStep3(order);
        if (!operatorReason.isSuccess()) return operatorReason;
        order.updatePureWeight(orderService.standards, true);
        operatorReason = orderService.updateStep4Total(order);
        return operatorReason;
    }

    @GetMapping("/delorder")
    public OperatorReason DeleteOrder(Order order) {
        if (order == null || order.getId() == 0) return OperatorReasonEnum.SUCCESS.get();
        return orderService.deleteOrder(order);
    }

    @GetMapping("/searchorder")
    public String searchOrder(Order order) {
        return orderService.searchOrderInStep1(order);
    }
}
