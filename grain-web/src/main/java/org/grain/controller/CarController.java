/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.controller;

import org.grain.common.domain.po.Car;
import org.grain.common.exception.OperatorReason;
import org.grain.service.CarService;
import org.grain.service.UserService;
import org.grain.utils.JsonConvert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Title: the TestController class.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@RestController
@RequestMapping("car")
public class CarController {
    @Resource
    private CarService carService;
    
    @GetMapping("/add")
    public OperatorReason add(Car car) {
        OperatorReason operatorReason = carService.check(car);
        if (!operatorReason.isSuccess()) return operatorReason;
        return carService.insert(car);
    }

    @GetMapping("/get")
    public String get(Car car) {
        Car query = carService.get(car.getCarId());
        return JsonConvert.serialList(Arrays.asList(query).stream().collect(Collectors.toList()));
    }
}
