/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.controller;

import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.service.UserService;
import org.grain.utils.JsonConvert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Title: the TestController class.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    private UserService userService;
    
    @GetMapping("/add")
    public OperatorReason add(User user) {
        return userService.add(user);
    }

    @GetMapping("/insert")
    public OperatorReason insert(User user) {
        OperatorReason operatorReason = userService.check(user);
        if (!operatorReason.isSuccess()) return operatorReason;
        return userService.insert(user);
    }

    @GetMapping("exist")
    public OperatorReason exist(String id) {
        if (userService.findById(id)) {
            return OperatorReasonEnum.SUCCESS.get();
        } else {
            return OperatorReasonEnum.FAIL.get();
        }
    }

    @GetMapping("/search")
    public String getUserList(String id) {
        List<User> users = userService.searchMatchUserById(id);
        return JsonConvert.serialList(users);
    }
}
