/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.controller;

import org.grain.common.domain.po.User;
import org.grain.mapper.CarMapper;
import org.grain.mapper.OrderMapper;
import org.grain.mapper.UserMapper;
import org.grain.utils.IJson;
import org.grain.utils.JsonConvert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Title: the TestController class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@RestController
public class ShowController {
    @Resource
    private CarMapper carMapper;
    
    @Resource
    private OrderMapper orderMapper;

    @Resource
    private UserMapper userMapper;
    @GetMapping
    public String helloword() {
        return "HELLO, WORLD";
    }
    
    @GetMapping("/user")
    public String getUserList() {
        List<User> users = userMapper.selectList(null);
        return serialList(users);
    }

    @GetMapping("/car")
    public String getCarList() {
        return serialList(carMapper.selectList(null));
    }
    
    @GetMapping("/order")
    public String getOrderList() {
        return serialList(orderMapper.selectList(null));
    }

    private static <T extends IJson> String serialList(List<T> serials) {
        return JsonConvert.serialList(serials);
    }
}
