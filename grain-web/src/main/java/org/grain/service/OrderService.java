/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.grain.api.StandardApi;
import org.grain.api.impl.StandardImpl;
import org.grain.common.constant.State;
import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.Order;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.mapper.CarMapper;
import org.grain.mapper.OrderMapper;
import org.grain.utils.JsonConvert;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

/**
 * Title: the OrderService class.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/3/25]
 * @since 2022/3/25
 */
@Slf4j
@Component
public class OrderService extends IService {
    @Resource
    private CarMapper carMapper;
    
    @Resource
    private OrderMapper orderMapper;
    
    public StandardApi standards = new StandardImpl();
    
    public OperatorReason checkStep1(Order order) {
        OperatorReason operatorReason = Order.checkStep1(order);
        if (!operatorReason.isSuccess()) return operatorReason;

        Order query = orderMapper.selectById(order.getId());
        if (query == null) {
            return OperatorReasonEnum.FAIL.get("order not exist!");
        }

        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public OperatorReason updateStep1(Order order) {
        order.setCreateTime(new Timestamp(System.currentTimeMillis()));
        order.setState(State.CHECK);
        int num = orderMapper.updateOrderStep1(order);
        return num == 1 ? OperatorReasonEnum.SUCCESS.get(order.getId()) :
                OperatorReasonEnum.INSERT_FAIL.get();
    }
    
    private void fillStep1(Order order) {
        order.setState(State.BEGIN);
        order.setId(getNextId());
    }
    
    public OperatorReason checkStep2(Order order) {
        OperatorReason operatorReason = Order.checkStep2(order, standards);
        if (!operatorReason.isSuccess()) return operatorReason;

        List<Car> cars = carMapper.selectCarByUserId(order.getCarId(), order.getOwner());
        if (cars.size() == 0) {
            return OperatorReasonEnum.FAIL.get("car not exist");
        }


        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public OperatorReason updateStep2(Order order) {
        fillStep1(order);
        order.setDoQualityTime(new Timestamp(System.currentTimeMillis()));
        int num = orderMapper.insert(order);
        if (num == 1) {
            return OperatorReasonEnum.SUCCESS.get(order.getId());
        }
        return OperatorReasonEnum.INSERT_FAIL.get();
    }
    
    public OperatorReason checkStep3(Order order) {
        OperatorReason operatorReason = Order.checkStep3(order);
        if (!operatorReason.isSuccess()) return operatorReason;
        
        Order query = orderMapper.selectById(order.getId());
        if (query == null) {
            return OperatorReasonEnum.FAIL.get("order not exist!");
        }
        if (query.getTotalWeight() <
                (order.getNoCarWeight() + order.getAdditionWeight())) {
            return OperatorReasonEnum.FAIL.get("no car + addition weight greatthan total!");
        }
        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public OperatorReason updateStep3(Order order) {
        order.setDoWeightTime(new Timestamp(System.currentTimeMillis()));
        order.setState(State.WEIGHT);
    
        int num = orderMapper.updateOrderStep3(order);
        updatePureWeight(order.getId());
        if (num == 1) {
            return OperatorReasonEnum.SUCCESS.get();
        }
        return OperatorReasonEnum.INSERT_FAIL.get();
    }

    public OperatorReason updateStep4Total(Order order) {
        int num = orderMapper.updateOrderTotal(order);
        if (num == 1) {
            return OperatorReasonEnum.SUCCESS.get();
        }
        return OperatorReasonEnum.INSERT_FAIL.get();
    }

    public OperatorReason deleteOrder(Order order) {
        try {
            orderMapper.deleteById(order.getId());
            return OperatorReasonEnum.SUCCESS.get();
        } catch (Exception exp) {
            return OperatorReasonEnum.FAIL.get(exp.toString());
        }
    }

    private void updatePureWeight(long id) {
        try {
            if (id == 0) return;
            Order order = orderMapper.selectById(id);
            order.updatePureWeight(standards);
            orderMapper.updateOrderStep4(order);
        } catch (Exception exp) {
            log.error("updatePureWeight failed!" + exp.toString());
        }
    }

    public String searchOrderInStep1(Order order) {
        if (order == null || (StringUtils.isEmpty(order.getOwner()) && order.getId() == 0)) {
            return JsonConvert.serialList(Collections.emptyList());
        }
        return JsonConvert.serialList(orderMapper.searchOrder(order));
    }
    @Override
    protected long firstGetId() {
        return orderMapper.getNextId();
    }
}
