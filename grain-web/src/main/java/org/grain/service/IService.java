/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.service;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Title: the IService interface.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/3/27]
 * @since 2022/3/27
 */
public abstract class IService {
    protected volatile AtomicLong orderIdGenerator = null;
    protected Object orderIdLock = new int[0];
    protected abstract long firstGetId();
    
    public long getNextId() {
        if (orderIdGenerator != null) {
            return orderIdGenerator.getAndIncrement();
        }
        synchronized (orderIdLock) {
            if (orderIdGenerator != null) {
                return orderIdGenerator.getAndIncrement();
            }
            orderIdGenerator = new AtomicLong(firstGetId());
            return orderIdGenerator.getAndIncrement();
        }
    }
}
