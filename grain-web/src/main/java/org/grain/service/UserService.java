/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.grain.common.constant.Constant;
import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.mapper.UserMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Title: the UserServiceImpl class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@Component
public class UserService {
    @Resource
    private UserMapper userMapper;

    public boolean findById(final String id) {
        User user = userMapper.selectById(id);
        return user != null;
    }

    public List<User> searchMatchUserById(final String id) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Constant.USER_ID, id);
        return userMapper.selectList(queryWrapper);
    }
    public OperatorReason check(User user) {
        if (!user.isValidUser()) {
            return OperatorReasonEnum.FAIL.get("id or name is empty");
        }
        return OperatorReasonEnum.SUCCESS.get();
    }

    public OperatorReason add(final User user) {
        if (user != null && user.getId() != null) {
            User query = userMapper.selectById(user.getId());
            if (query != null) {
                return OperatorReasonEnum.SUCCESS.get("already exist!");
            }
        }
        return insert(user);
    }

    public OperatorReason insert(final User user) {
        int insertNum = userMapper.insert(user);
        if (insertNum != 1) {
            return OperatorReasonEnum.INSERT_FAIL.get();
        }
        return OperatorReasonEnum.SUCCESS.get();
    }
}
