/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.service;

import org.grain.common.domain.po.Car;
import org.grain.common.domain.po.User;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.mapper.CarMapper;
import org.grain.mapper.UserMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Title: the UserServiceImpl class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@Component
public class CarService extends IService {
    @Resource
    private CarMapper carMapper;
    
    @Resource
    private UserMapper userMapper;
    
    public OperatorReason check(Car car) {
        OperatorReason reason = Car.check(car);
        if (!reason.isSuccess()) return reason;
        
        User user = userMapper.selectById(car.getUserId());
        if (user == null) {
            return OperatorReasonEnum.FAIL_NULL.get("user_id not exist");
        }
        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public OperatorReason insert(final Car car) {
        car.setCarId(getNextId());
        int insertNum = carMapper.insert(car);
        if (insertNum != 1) {
            return OperatorReasonEnum.INSERT_FAIL.get();
        }
        return OperatorReasonEnum.SUCCESS.get(car.getCarId());
    }

    public Car get(final long carId) {
        return carMapper.selectById(carId);
    }
    
    @Override
    protected long firstGetId() {
        return carMapper.getNextId();
    }
}
