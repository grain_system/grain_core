package org.grain;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "org.grain.**")
@MapperScan("org.grain.mapper")
@EnableScheduling
@EnableAutoConfiguration
@EnableAsync
public class GrainSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainSystemApplication.class, args);
    }
}
