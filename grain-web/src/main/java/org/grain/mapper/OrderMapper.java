/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.grain.common.domain.po.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Title: the OrderMapper interface.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public interface OrderMapper extends BaseMapper<Order> {
    long getNextId();
    int updateOrderStep1(Order order);
    int updateOrderStep3(Order order);
    int updateOrderStep4(Order order);
    int updateOrderTotal(Order order);

    List<Order> searchOrder(Order order);
}
