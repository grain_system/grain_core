/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.grain.common.domain.po.User;
import org.springframework.stereotype.Component;

/**
 * Title: the UserMapper class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public interface UserMapper extends BaseMapper<User> {
}
