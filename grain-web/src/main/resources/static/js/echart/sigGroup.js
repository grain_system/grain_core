let myChart = echarts.init(document.getElementById('sigGroupIssue'));

$.getJSON("/sig_group", function (json) {
    const signGroupName = [];
    const totalIssueNumber = [];
    const demandNumber = [];

    let data = json.data;
    for (let i = 0; i < data.length; i++) {
        let obj = data[i];
        let totalIssues = obj.totalIssueNumber;
        let demands = obj.demandNumber;
        if (!(totalIssues === 0 && demands === 0)) {
            signGroupName.push(obj.sigGroupName);
            totalIssueNumber.push(totalIssues);
            demandNumber.push(demands);
        }
    }

    myChart.setOption({
        title: {
            text: '问题指标统计'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            show: true,
            position: 'right'
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        yAxis: {
            type: 'category',
            data: signGroupName
        },
        series: [
            {
                name: '总缺陷数',
                type: 'bar',
                data: totalIssueNumber,
                label: {
                    show: true,
                    position: 'right'
                }
            },
            {
                name: '总需求数',
                type: 'bar',
                data: demandNumber,
                label: {
                    show: true,
                    position: 'right'
                }
            }
        ]
    })
})