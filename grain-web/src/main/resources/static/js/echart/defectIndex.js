let myChart = echarts.init(document.getElementById('defectIndex'));

$.getJSON("/defect_index", function (json) {
    const deteStr = [];
    const currentDefectIndex = [];
    const cumulativeSubmitted = [];
    const cumulativeSolved = [];

    let data = json.data;
    for (let i = 0; i < data.length; i++) {
        let obj = data[i];
        deteStr.push(obj.dateStr)
        currentDefectIndex.push(obj.currentDefectIndex)
        cumulativeSubmitted.push(obj.cumulativeSubmitted)
        cumulativeSolved.push(obj.cumulativeSolved)
    }

    myChart.setOption({
        title: {
            text: 'DI三曲线'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: ['current DI', 'cumulative submitted', 'cumulative solved']
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: deteStr
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'current DI',
                type: 'line',
                areaStyle: {
                    color: "#feeab3"
                },
                emphasis: {
                    focus: 'series'
                },
                data: currentDefectIndex
            },
            {
                name: 'cumulative submitted',
                type: 'line',
                areaStyle: {
                    color: "#b2e5ff"
                },
                emphasis: {
                    focus: 'series'
                },
                data: cumulativeSubmitted
            },
            {
                name: 'cumulative solved',
                type: 'line',
                areaStyle: {
                    color: "#ffd7d7"
                },
                emphasis: {
                    focus: 'series'
                },
                data: cumulativeSolved
            }
        ]
    })
})