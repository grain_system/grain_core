let myChart2 = echarts.init(document.getElementById('assigneeIssue'));

$.getJSON("/issues/assignee", function (json) {
    const assigneeName = [];
    const totalIssueNumber = [];
    const criticalIssueNumber = [];
    const increaseIssueNumber = [];
    const DI = [];
    const demandNumber = [];
    const consultNumber = [];
    const taskNumber = [];
    let data = json.data;
    for (let i = 0; i < data.length; i++) {
        let obj = data[i];
        assigneeName.push(obj.assigneeUserName)
        totalIssueNumber.push(obj.totalIssueNumber)
        criticalIssueNumber.push(obj.criticalIssueNumber)
        increaseIssueNumber.push(obj.increaseIssueNumber)
        DI.push(obj.weight)
        demandNumber.push(obj.demandNumber)
        consultNumber.push(obj.consultNumber)
        taskNumber.push(obj.taskNumber)
    }
    myChart2.setOption({
        title: {text: '问题指标统计'},
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {},
        grid: {
            left: '3%',
            right: '4%',
            bottom: '20px',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: assigneeName,
                axisLabel: {
                    interval: 0,
                    rotate: "45"
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量（个）',
                min: 0,
                max: 200
            }
        ],
        series: [
            {
                name: '总缺陷数',
                type: 'bar',
                stack: 'Ad',
                emphasis: {
                    focus: 'series'
                },
                data: totalIssueNumber
            },
            {
                name: '严重缺陷数',
                type: 'bar',
                stack: 'Ad',
                emphasis: {
                    focus: 'series'
                },
                data: criticalIssueNumber
            },
            {
                name: '新增缺陷数',
                type: 'bar',
                stack: 'Ad',
                emphasis: {
                    focus: 'series'
                },
                data: increaseIssueNumber
            },
            {
                name: '当前DI',
                type: 'bar',
                emphasis: {
                    focus: 'series'
                },
                data: DI
            },
            {
                name: '需求数',
                type: 'bar',
                stack: 'not defect',
                emphasis: {
                    focus: 'series'
                },
                data: demandNumber,
            },
            {
                name: '咨询数',
                type: 'bar',
                stack: 'not defect',
                emphasis: {
                    focus: 'series'
                },
                data: consultNumber
            },
            {
                name: '任务数',
                type: 'bar',
                stack: 'not defect',
                emphasis: {
                    focus: 'series'
                },
                data: taskNumber
            }
        ]
    });
})
