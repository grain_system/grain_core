/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */

import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.api.impl.StandardImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * Title: the StardandTest class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public class StardandImpurityTest {
    StandardApi standard = new StandardImpl();
    
    @Test
    public void testLevel1_moisture() {
        int bulkDensity = 800;
        int level = standard.getLevel(bulkDensity);
        Assert.assertEquals(0, level);
        StandardLevelApi standardLevelApi = standard.getStandard(level);
        double[][] inputs = {
                {0.00, 1.5},
                {0.01, 0.75},
                {0.49, 0.75},
                {0.5, 0.75},
                {0.6, 0.00},
                {1.4, 0.00},
                {1.5, -1.5},
                {1.9, -1.5},
                {2.0, -3.0},
                {2.4, -3.0},
                {2.5, -4.5},
                {2.9, -4.5},
                {3.0, -6.0},
                {3.01, -6.0},
                {3.4, -6.0},
                {3.5, -7.5},
        };
        int cnt = 0;
        for (double[] input: inputs) {
            System.out.println("cnt = " + cnt);
            Assert.assertEquals(input[1], standardLevelApi.reCalcImpurity(input[0]), 0.001);
            cnt += 1;
        }
    }
}
