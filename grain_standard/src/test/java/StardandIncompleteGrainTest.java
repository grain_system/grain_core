/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */

import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.api.impl.StandardImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * Title: the StardandTest class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public class StardandIncompleteGrainTest {
    StandardApi standard = new StandardImpl();
	int[] bulkDensity = {790, 770, 750, 730, 710};
	int[] compareLevel = {0, 1, 2, 3, 4};
    
    @Test
    public void testLevel1_IncompleteGrain() {
    	int count = 0;
    	for (int curBulkDensity: bulkDensity) {
			int level = standard.getLevel(curBulkDensity);
			Assert.assertEquals(count ++, level);
			StandardLevelApi standardLevelApi = standard.getStandard(level);
			double base = standardLevelApi.getIncompleteGrain();
			double[][] inputs = {
					{base + 0.1, 0.0},
					{base + 0.9, 0.0},
					{base + 1.0, -0.5},
					{base + 1.9, -0.5},
					{base + 2.0, -1.0},
					{base + 2.9, -1.0},
					{base + 3.1, -1.5},
					{base + 3.9, -1.5},
					{base + 4.0, -2.0},
					{base + 4.9, -2.0},
					{base - 0.9, 0.0},
					{base - 1.0, 0.0},
					{base - 1.1, 0.0},
					{base - 2.1, 0.0},
					{base - 3.1, 0.0}
			};
			int cnt = 0;
			for (double[] input: inputs) {
				System.out.println("cnt = " + cnt);
				Assert.assertEquals(input[1], standardLevelApi.reCalcIncompleteGrain(input[0]), 0.001);
				cnt += 1;
			}
    	}
    }
}
