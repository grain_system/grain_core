/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */

import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.api.impl.StandardImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * Title: the StardandTest class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public class StardandMoistureTest {
    StandardApi standard = new StandardImpl();
    
    @Test
    public void testLevel1_moisture() {
        int bulkDensity = 800;
        int level = standard.getLevel(bulkDensity);
        Assert.assertEquals(0, level);
        StandardLevelApi standardLevelApi = standard.getStandard(level);
        double[][] inputs = {
                {12.1, 0},
                {12.9, 0},
                {11.51, 0.75},
                {11.99, 0.75},
                {12.0, 0.0},
                {12.01, 0.0},
                {12.49, 0.0},
                {12.51, 0.0},
                {13.0, 0.0},
                {13.01, -1.0},
                {13.49, -1.0},
                {13.50, -1.0},
                {13.51, -2.0}
        };
        int cnt = 0;
        for (double[] input: inputs) {
            System.out.println("cnt = " + cnt);
            Assert.assertEquals(input[1], standardLevelApi.reCalcMoisture(input[0]), 0.001);
            cnt += 1;
        }
    }
}
