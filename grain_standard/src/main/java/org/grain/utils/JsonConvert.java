package org.grain.utils;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JsonConvert {
    public static <T extends IJson> String serialList(List<T> objs) {
        JSONArray jsonRootArray = new JSONArray();
        if (objs != null && !objs.isEmpty()) {
            for (IJson obj: objs) {
                jsonRootArray.add(obj.toJsonObject());
            }
        }
        return jsonRootArray.toString();
    }

    public static <T> List<T> deserialList(Class<T> clz, JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return Collections.emptyList();
        }
        List<T> objs = new ArrayList<>();
        for (JSONObject jsonObject: jsonArray.jsonIter()) {
            objs.add(jsonObject.toBean(clz));
        }
        return objs;
    }
}
