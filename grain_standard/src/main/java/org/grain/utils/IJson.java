package org.grain.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.util.HashMap;
import java.util.Map;

public interface IJson {
    default JSONObject toJsonObject() {
        return JSONUtil.parseObj(this);
    }

    default Map<String, Object> toMapObject() {
        JSONObject jsonObject = toJsonObject();
        Map<String, Object> mapObj = new HashMap<>(jsonObject.size());
        for (String key: jsonObject.keySet()) {
            mapObj.put(key, jsonObject.get(key));
        }
        return mapObj;
    }
}
