/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.constant;

/**
 * Title: the Constant class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public enum Level {
    LEVEL_1,
    LEVEL_2,
    LEVEL_3,
    LEVEL_4,
    LEVEL_5
}