/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.domain.po;

/**
 * Title: the Car class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.utils.IJson;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("grain_car")
public class Car implements IJson {
    @TableId
    @TableField(value="car_id")
    private long carId;
    
    @TableField(value="card_num")
    private String cardNum;
    
    @TableField(value = "user_id")
    private String userId;
    
    @TableField(value = "car_type")
    private String carType;
    
    @TableField(value = "car_color")
    private String carColor;
    
    @TableField(value = "car_desc")
    private String carDesc;
    
    public static OperatorReason check(Car car) {
        if (car == null) {
            return OperatorReasonEnum.FAIL_NULL.get();
        }
        
        if (car.getCarId() != 0) {
            return OperatorReasonEnum.FAIL.get("carId invalid");
        }
        
        if (StringUtils.isEmpty(car.getCardNum())
                && StringUtils.isEmpty(car.getCarDesc())) {
            return OperatorReasonEnum.FAIL_NULL.get("car num and desc both null");
        }
        
        if (StringUtils.isEmpty(car.getUserId())) {
            return OperatorReasonEnum.FAIL_NULL.get("user id empty");
        }
        
        return OperatorReasonEnum.SUCCESS.get();
    }
}
