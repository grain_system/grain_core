/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.domain.po;

/**
 * Title: the User class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.grain.common.constant.Sex;
import org.grain.utils.IJson;

import java.sql.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("grain_user")
public class User implements IJson {
    @TableId
    private String id;
    
    private String name;
    
    private String name1;
    
    @EnumValue
    private Sex sex;
    
    private String nation;
    
    private Date birthday;
    
    private String address;
    
    private String signer;
    
    @TableField(value = "valid_start")
    private Date validStart;
    
    @TableField(value = "valid_end")
    private Date validEnd;
    
    public boolean isValidUser() {
        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(name)) {
            return false;
        }
        if (id.length() != 18 || StringUtils.countMatches(id, 'X') > 1) {
            return false;
        }
        return true;
    }
}
