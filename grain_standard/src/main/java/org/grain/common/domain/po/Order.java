/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.domain.po;

/**
 * Title: the User class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;
import org.grain.common.constant.Constant;
import org.grain.common.constant.State;
import org.grain.common.exception.OperatorReason;
import org.grain.common.exception.OperatorReasonEnum;
import org.grain.utils.IJson;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("grain_order")
public class Order implements IJson {
    @TableId
    private long id;
    
    private String owner;
    
    @TableField(value = "create_time")
    private Timestamp createTime;
    
    @TableField
    @EnumValue
    private State state;
    
    @TableField(value = "car_id")
    private long carId;
    
    @TableField(value = "total_weight")
    private double totalWeight;
    
    @TableField(value = "do_weight_time")
    private Timestamp doWeightTime;
    
    @TableField(value = "no_car_weight")
    private double noCarWeight;
    
    @TableField(value = "addition_weight")
    private double additionWeight;
    
    @TableField(value = "bulk_density")
    private int bulkDensity;
    
    @TableField(value = "incomplete_grain")
    private double incompleteGrain;

    @TableField(value = "incomplete_grain_weight_per")
    private double incompleteGrainWeightPer;

    private double impurity;

    @TableField(value = "impurity_weight_per")
    private double impurityWeightPer;

    private double moisture;

    @TableField(value = "moisture_weight_per")
    private double moistureWeightPer;

    @TableField(value = "mildew_rate")
    private double mildewRate;

    @TableField(value = "do_quality_time")
    private Timestamp doQualityTime;

    @TableField(value = "incr_total_weight")
    private double incrTotalWeight;

    @TableField(value = "desc_total_weight")
    private double descTotalWeight;

    @TableField(value = "pure_total_weight")
    private double pureTotalWeight;

    public void updatePureWeight(StandardApi standardApi) {
        updatePureWeight(standardApi, false);
    }
    public void updatePureWeight(StandardApi standardApi, boolean forceUpdate) {
        if (forceUpdate || Constant.equal(pureTotalWeight, 0.0)) {
            updatePureWeight(standardApi, this);
        }
    }

    private static void updatePureWeight(StandardApi standardApi, Order order) {
        int level = standardApi.getLevel(order.getBulkDensity());
        StandardLevelApi standardLevelApi = standardApi.getStandard(level);

        double moisturePercent = standardLevelApi.reCalcMoisture(order.getMoisture());
        order.setMoistureWeightPer(moisturePercent);

        double impurityInput = standardLevelApi.reCalcImpurity(order.getImpurity());
        order.setImpurityWeightPer(impurityInput);

        double incompleteGrain = standardLevelApi.reCalcIncompleteGrain(order.getIncompleteGrain());
        order.setIncompleteGrainWeightPer(incompleteGrain);

        double mildewRate = - order.getMildewRate();

        double subTotalWeight = order.getTotalWeight() - order.getNoCarWeight() - order.getAdditionWeight();

        double totalDecWeight = 0.0;
        double totalIncWeight = 0.0;
        double calcMoistureWeight = moisturePercent * subTotalWeight / 100.0;
        if (moisturePercent <= 0.0) {
            totalDecWeight -= calcMoistureWeight;
        } else {
            totalIncWeight += calcMoistureWeight;
        }

        double calcImpurityWeight = impurityInput * subTotalWeight / 100.0;
        if (impurityInput <= 0.0) {
            totalDecWeight -= calcImpurityWeight;
        } else {
            totalIncWeight += calcImpurityWeight;
        }

        double calcIncompleteGrainWeight = incompleteGrain * subTotalWeight / 100.0;
        totalDecWeight -= calcIncompleteGrainWeight;

        if (!Constant.equal(mildewRate, 0.0)) {
            totalDecWeight -= mildewRate * subTotalWeight / 100.0;
        }

        order.setIncrTotalWeight(totalIncWeight);
        order.setDescTotalWeight(totalDecWeight);

        double pureWeight = subTotalWeight - totalDecWeight + totalIncWeight;
        order.setPureTotalWeight(pureWeight);
    }

    public static OperatorReason checkStep1(Order order) {
        if (order == null) {
            return OperatorReasonEnum.FAIL_NULL.get();
        }
        if (order.getId() == 0) {
            return OperatorReasonEnum.FAIL.get("ID invalid");
        }

        if (order.getTotalWeight() <= 0.0) {
            return OperatorReasonEnum.FAIL.get("total weight invalid");
        }

        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public static OperatorReason checkStep2(Order order, StandardApi standards) {
        if (order == null) {
            return OperatorReasonEnum.FAIL_NULL.get();
        }

        if (StringUtils.isEmpty(order.getOwner())) {
            return OperatorReasonEnum.FAIL.get("owner is invalid");
        }

        int level = standards.getLevel(order.getBulkDensity());
        if (!standards.validLevel(level)) {
            return OperatorReasonEnum.FAIL_INVALID_PARAM.get("bulkDensity invalid");
        }
        if (order.getIncompleteGrain() < 0.0 || order.getIncompleteGrain() > 100.0) {
            return OperatorReasonEnum.FAIL_INVALID_PARAM.get("incompleteGrain invalid");
        }
        if (order.getImpurity() < 0.0 || order.getImpurity() > 100.0) {
            return OperatorReasonEnum.FAIL_INVALID_PARAM.get("impurity invalid");
        }
        if (order.getMoisture() < 0.0 || order.getMoisture() > 100.0) {
            return OperatorReasonEnum.FAIL_INVALID_PARAM.get("mositure invalid");
        }
        if (order.getMildewRate() < 0.0 || order.getMildewRate() > 100.0) {
            return OperatorReasonEnum.FAIL_INVALID_PARAM.get("mildew_rate invalid");
        }

        return OperatorReasonEnum.SUCCESS.get();
    }
    
    public static OperatorReason checkStep3(Order order) {
        if (order == null) {
            return OperatorReasonEnum.FAIL_NULL.get();
        }
        if (order.getId() == 0) {
            return OperatorReasonEnum.FAIL.get("id not valid");
        }

        if (order.getNoCarWeight() < 0.0) {
            return OperatorReasonEnum.FAIL.get("no car weight invalid");
        }
        if (order.getAdditionWeight() < 0.0) {
            return OperatorReasonEnum.FAIL.get("addition invalid");
        }
        
        return OperatorReasonEnum.SUCCESS.get();
    }
}
