/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.constant;

/**
 * Title: the Constant class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
public class Constant {
    public static final String USER_ID = "id";
    public static final double DOUBLE_EQUAL_COMPARE = 0.00001;

    public static boolean equal(double src, double cmp) {
        return Math.abs(src - cmp) < DOUBLE_EQUAL_COMPARE;
    }
}
