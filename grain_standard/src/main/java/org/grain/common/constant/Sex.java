/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.constant;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;

/**
 * Title: the Sex Enum.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@Getter
public enum Sex implements IEnum<Integer> {
    MALE(1, "\u7537"),
    FEMALE(0, "\u5973");
    
    private int code;
    
    private String desc;
    
    Sex(final int code, final String desc) {
        this.code = code;
        this.desc = desc;
    }
    
    public static Sex getSexFromCode(String strSex) {
        if (FEMALE.name().equals(strSex)) {
            return FEMALE;
        }
        if ("0".equals(strSex)) {
            return FEMALE;
        }
        return MALE;
    }
    
    @Override
    public Integer getValue() {
        return code;
    }
}
