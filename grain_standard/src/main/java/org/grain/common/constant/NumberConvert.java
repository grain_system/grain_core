package org.grain.common.constant;

import java.text.DecimalFormat;

public class NumberConvert {
    private final static String[] STR_NUMBER = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    private final static String[] STR_UNIT = {"", "拾", "佰", "仟", "萬", "拾", "佰", "仟", "亿", "拾", "佰", "仟", "萬"};
    private final static String[] STR_UNIT2 = {"角", "分", "厘"};

    public static String convert(double d){
        DecimalFormat df = new DecimalFormat("#0.###");
        String strNum = df.format(d);

        if(strNum.indexOf(".") != -1){
            //存在"."表示为小数
            String num = strNum.substring(0, strNum.indexOf("."));
            if(num.length() > 13){
                //本程序整数部分只取13位数以内的。
                System.out.println("数字过大，本程序不适用！");
                return "";
            }
        }
        String point = "";
        if(strNum.indexOf(".") != -1){
            point = "公斤";
        }else{
            point = "公斤整";
        }
        String result = getInteger(strNum) + point + getDecimal(strNum);

        //如果程序以“圆”开头，则取“圆”后面的字符串
        if(result.startsWith("斤")){
            result = result.substring(1, result.length());
        }
        return result;
    }

    private static String getInteger(String num){
        if(num.indexOf(".") != -1){
            num = num.substring(0, num.indexOf("."));
        }
        num = new StringBuffer(num).reverse().toString();
        StringBuffer temp = new StringBuffer();
        for(int i=0; i<num.length(); i++){
            temp.append(STR_UNIT[i]);
            temp.append(STR_NUMBER[num.charAt(i) - 48]);
        }
        num = temp.reverse().toString();
        //替换字符串中的不符合事实的字符
        num = numReplace(num, "零拾", "零");
        num = numReplace(num, "零佰", "零");
        num = numReplace(num, "零仟", "零");
        num = numReplace(num, "零萬", "萬");
        num = numReplace(num, "零亿", "亿");
        num = numReplace(num, "零零", "零");
        num = numReplace(num, "亿万", "亿");
        if(num.lastIndexOf("零") == num.length() - 1) {
            num = num.substring(0, num.length() - 1);
        }
        return num;
    }
    private static String getDecimal(String num){
        if(num.indexOf(".") == -1){
            return "";
        }
        num = num.substring(num.indexOf(".") + 1);
        num = new StringBuffer(num).toString();
        StringBuffer temp = new StringBuffer();
        for(int i=0; i<num.length(); i++){
            temp.append(STR_NUMBER[num.charAt(i) - 48]);
            temp.append(STR_UNIT2[i]);
        }
        num = temp.toString();
        num = numReplace(num, "零角", "零");
        num = numReplace(num, "零分", "零");
        num = numReplace(num, "零厘", "零");
        num = numReplace(num, "零零", "零");
        if(num.lastIndexOf("零") == num.length() - 1){
            num = num.substring(0, num.length() - 1);
        }
        return num;
    }

    private static String numReplace(String num, String oldStr, String newStr){
        while(true){
            if(num.indexOf(oldStr) == -1){
                break;
            }
            num = num.replaceAll(oldStr, newStr);
        }
        return num;
    }
}
