/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.constant;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;

/**
 * Title: the Sex Enum.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/2/26]
 * @since 2022/2/26
 */
@Getter
public enum State implements IEnum<Integer> {
    BEGIN(4)
    ,CHECK(3)
    ,WEIGHT(2)
    ,INVALID(-1)
    ;
    
    
    private int code;
    
    State(final int code) {
        this.code = code;
    }
    
    public static State getEnumFromCode(String enumStr) {
        for (State state: State.values()) {
            if (state.name().equals(enumStr) || ("" + state.code).equals(enumStr)) {
                return state;
            }
        }
        return INVALID;
    }
    
    @Override
    public Integer getValue() {
        return code;
    }
}
