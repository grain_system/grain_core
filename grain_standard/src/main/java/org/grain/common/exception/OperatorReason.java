/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.exception;

/**
 * Title: the OperatorReason interface.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/3/25]
 * @since 2022/3/25
 */
public interface OperatorReason {
    void setCode(int code);
    void setReason(Object reason);
    Object getReason();
    boolean isSuccess();
}
