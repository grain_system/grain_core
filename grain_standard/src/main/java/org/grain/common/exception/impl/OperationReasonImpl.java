/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.exception.impl;

import cn.hutool.json.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.grain.common.exception.OperatorReason;

/**
 * Title: the OperationReasonImpl class.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/3/25]
 * @since 2022/3/25
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OperationReasonImpl implements OperatorReason {
    public static final int SUCCESS_CODE = 0;
    protected int code = SUCCESS_CODE;
    protected Object reason = "SUCCESS";
    
    @Override
    public boolean isSuccess() {
        return code == SUCCESS_CODE;
    }
    
    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOpt("code", code);
        jsonObject.putOpt("reason", reason);
        return jsonObject.toString();
    }

    public static OperationReasonImpl build(JSONObject jsonObject) {
        OperationReasonImpl opr = new OperationReasonImpl();
        opr.setCode(jsonObject.getInt("code"));
        opr.setReason(jsonObject.getStr("reason"));
        return opr;
    }
}
