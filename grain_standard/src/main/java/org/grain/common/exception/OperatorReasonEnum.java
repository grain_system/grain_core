/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.common.exception;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import org.grain.common.exception.impl.OperationReasonImpl;

/**
 * Title: the OperatorReasonEnum Enum.
 * <p>
 * Description:
 *
 * @author justbk
 * @version [issueManager 0.0.1, 2022/3/25]
 * @since 2022/3/25
 */
public enum OperatorReasonEnum {
    SUCCESS(0, "SUCCESS"),
    FAIL(-1, "FAIL"),
    INSERT_FAIL(-2, "INSERT FAIL"),
    FAIL_NULL(-3, "OBJECT NULL"),
    FAIL_INVALID_PARAM(-4, "INVALID PARAM"),
    EXCEPTION(-5, "WITH EXCEPTION"),
    UNKNOWN(-1000, "UNKNOW ERROR")
    ;
    public final int code;
    public final String reason;
    OperatorReasonEnum(final int code, final String reason) {
        this.code = code;
        this.reason = reason;
    }
    
    public OperatorReason get() {
        return new OperationReasonImpl(code, reason);
    }
    
    public OperatorReason get(Object reason) {
        OperatorReason or = get();
        or.setReason(reason);
        return or;
    }
}
