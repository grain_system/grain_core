/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.api;

/**
 * Title: the StandardLevelApi interface.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public interface StandardLevelApi {
    int getBulkDensity();
    int getLevel();
    double getIncompleteGrain();
    double getImpurity();
    double getMoisture();
    /**
     * Recalc the water total weight coef.
     * @param moisture the water weight coef.
     * @return the coef
     * */
    default double reCalcMoisture(double moisture) {
        return reCalcCoef(getMoisture(), moisture, 0.5,
                0.5, 0.75, -1.0);
    }
    
    /**
     * Recalc the imputiry total coef.
     * @param impurity the imputiry
     * @return the coef
     */
    default double reCalcImpurity(double impurity) {
        return reCalcCoef(getImpurity(), impurity, 0.5,
                0.5, 0.75, -1.5);
    }
    
    default double reCalcIncompleteGrain(double incompleteGrain) {
        return reCalcCoef(getIncompleteGrain(), incompleteGrain, 1.0,
                1.0, 0.0, -0.5);
    }
    
    static double reCalcCoef(double input, double standard, double ignore,
                                 double step, double greatCoef, double lessCoef) {
        double standardCoef = input > standard ? greatCoef : lessCoef;
        double sub = Math.abs(input - standard);
        double retCoef = 0.0;
        if (sub < ignore) return retCoef;
        sub -= step;
        while (sub >= 0) {
            retCoef += standardCoef;
            sub -= step;
        }
        return retCoef;
    }
}
