/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.api.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.grain.api.StandardLevelApi;

/**
 * Title: the StandardLevelImpl class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
@AllArgsConstructor
@Getter
public class StandardLevelImpl implements StandardLevelApi {
    int bulkDensity;
    int level;
    double incompleteGrain;
    double impurity;
    double moisture;
}
