/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.api.impl;

import org.grain.api.StandardApi;
import org.grain.api.StandardLevelApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Title: the StandardImpl class.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public class StandardImpl implements StandardApi {
    List<StandardLevelApi> standards = new ArrayList<>(5);
    
    public StandardImpl() {
        standards.add(new StandardLevelImpl(790, 0, 6.0, 1.0, 12.5));
        standards.add(new StandardLevelImpl(770, 1, 6.0, 1.0, 12.5));
        standards.add(new StandardLevelImpl(750, 2, 8.0, 1.0, 12.5));
        standards.add(new StandardLevelImpl(730, 3, 8.0, 1.0, 12.5));
        standards.add(new StandardLevelImpl(710, 4, 10.0, 1.0, 12.5));
    }
    @Override
    public Integer getLevel(final int bulkDensity) {
        int level = 0;
        for (StandardLevelApi standard: standards) {
            if (bulkDensity >= standard.getBulkDensity()) {
                return level;
            }
            level ++;
        }
        return level;
    }
    
    @Override
    public boolean validLevel(final int level) {
        return level >= 0 && level < standards.size();
    }
    
    @Override
    public StandardLevelApi getStandard(final int level) {
        return standards.get(level);
    }
}
