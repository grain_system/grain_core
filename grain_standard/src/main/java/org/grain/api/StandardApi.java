/*
 * Copyright (c) @ justbk. 2021-2031. All rights reserved.
 */
package org.grain.api;

import java.util.List;

/**
 * Title: the StandardApi interface.
 * <p>
 * Description:
 *
 * @author Administrator
 * @version [issueManager 0.0.1, 2022/3/21]
 * @since 2022/3/21
 */
public interface StandardApi {
    /**
     * Get the level from bulk density.
     * @param  bulkDensity the bulkDensity
     * @return the level, start 0.
     */
    Integer getLevel(int bulkDensity);
    
    /**
     * If the level legal.
     * @param level the input level.
     * @return true if valid else false.
     * */
    boolean validLevel(int level);
    
    /**
     * Get standard by level.
     * @param level the level from getLevel
     * @return the standard
     */
    StandardLevelApi getStandard(int level);
    
}
